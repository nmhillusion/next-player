package nextplayer;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.MapChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;

/**
 *
 * @author nmhillusion
 */
final class Track {

    private String title, artist, album, year, composer, source, duration;
    private Label lblTitle, lblArtist;
    private ItemRow rowList;
    private int w, h;
    private int idx, wait = 0;
    private float opaIntro;
    private static int serial = 0;
    private boolean isVideo = false;
    private Image cover;
    private javax.swing.Timer timer;
    private static NextPlayer rootPlayer;
    private static VBox vbox;
    private static Group root;

    /**
     * Khởi tạo một track
     *
     * @param source đường dẫn tập tin của track
     * @param meta dữ liệu meta của track
     * @param title tiêu đề của track
     * @param duration thời gian chạy của track
     * @param offY track cách lề bao nhiêu
     * @param w chiều rộng ứng dụng
     * @param h chiều cao ứng dụng
     * @param info bảng chứa thông tin của track đang chơi
     */
    public Track(Media media, int w, int h, PlaylistNowPlaying pl) {
        this.w = w;
        this.h = h;
        source = media.getSource();

        if (media.getSource().contains("MP4") || media.getSource().contains("mp4")) {
            isVideo = true;
        }

        idx = serial;
        addMetadata(media, pl);
    }

    private String getValueOfMetadataValue(Object value) {
        return value.toString().trim().length() > 0 ? value.toString().trim() : "Đang cập nhật";
    }

    private void addMetadata(Media media, PlaylistNowPlaying pl) {
        duration = artist = album = year = composer = "Đang cập nhật";
        try {
            String fileName = new File(media.getSource()).getName();
            fileName = URLDecoder.decode(fileName, "utf-8");
            title = fileName.substring(0, fileName.lastIndexOf("."));
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Track.class.getName()).log(Level.SEVERE, null, ex);
            title = "Đang cập nhật";
        }

        addToListPane(pl);
        media.getMetadata().addListener((MapChangeListener.Change<? extends String, ? extends Object> change) -> {
            updateMetadata(change.getKey(), change.getValueAdded());
        });
        media.durationProperty().addListener((observable, oldValue, newValue) -> {
            updateMetadata("duration", newValue);
        });
    }

    public void updateMetadata(String key, Object value) {
        switch (key) {
            case "image":
                cover = (Image) value;
                break;
            case "title":
                String tmp = getValueOfMetadataValue(value);
                if (!tmp.equals("Đang cập nhật")) {
                    title = tmp;
                }
                rowList.setTitle(title);
                break;
            case "artist":
                artist = getValueOfMetadataValue(value);
                rowList.setArtist(artist);
                break;
            case "album":
                album = getValueOfMetadataValue(value);
                break;
            case "year":
                year = getValueOfMetadataValue(value);
                break;
            case "composer":
                composer = getValueOfMetadataValue(value);
                break;
            case "duration":
                double seconds = Double.parseDouble(value.toString());
                if (seconds > 0) {
                    duration = Method.toTime((int)seconds);
                    rowList.setDuration(duration);
                }
                break;
        }
        setInfo();
    }

    /**
     * Khởi tạo lại tham cho lớp
     */
    protected static void init(VBox v, Group _root, NextPlayer _rootPlayer) {
        serial = 0;
        vbox = v;
        root = _root;
        rootPlayer = _rootPlayer;
    }

    /**
     * đặt lại số thứ tự cho track
     *
     * @param value số thứ tự mới của track
     */
    protected void setNumber(int value) {
        rowList.setNumber("[" + value + "]. ");
    }

    /**
     * Đặt thông tin của track này lên bảng thông tin (khi được chơi)
     */
    protected String setInfo() {
        return 
                "[title]: " + Method.limitString(title, 55)
                + "\n[artist]: " + Method.limitString(artist, 55)
                + "\n[composer]: " + Method.limitString(composer, 55)
                + "\n[album]: " + Method.limitString(album, 55)
                + "\n[year]: " + Method.limitString(year, 55);
    }

    /**
     * @return nguồn link của track này
     */
    protected String getSource() {
        return source;
    }

    /**
     * @return media được tạo từ track này
     */
    protected Media getMedia() {
        return new Media(source);
    }

    /**
     *
     * @return Tiêu đề track
     */
    protected String getTitle() {
        return title;
    }

    /**
     *
     * @return Nghệ sĩ thể hiện track này
     */
    protected String getArtist() {
        return artist;
    }

    /**
     *
     * @return ảnh cover của track
     */
    protected Image getCover() {
        return cover;
    }

    public String getAlbum() {
        return album;
    }

    public String getYear() {
        return year;
    }

    public String getComposer() {
        return composer;
    }

    /**
     * @return có là video hay không
     */
    protected boolean isVideo() {
        return isVideo;
    }

    /**
     * Thêm track này vào danh sách chơi qua itemRow
     *
     * @param pl thể hiện của danh sách nhạc đang chơi
     */
    private void addToListPane(PlaylistNowPlaying pl) {
        rowList = new ItemRow("[" + (++serial) + "]. ", Method.limitString(title, 75) + " | " + Method.limitString(artist, 40), duration);
        rowList.setup(w - 45, 25, pl, createContextMenu(pl), this);
        vbox.getChildren().add(rowList);
//        System.out.println(" >>>> add to list pane <<<< " + rowList);
    }

    /**
     * Cài đặt trạng thái đang được chơi
     */
    void playing() {
        rowList.setPlaying();
    }

    /**
     * @return track này có đang được chọn không
     */
    protected boolean isSelected() {
        return rowList.isSelected();
    }

    /**
     * Hủy chọn track này
     */
    protected void unSelect() {
        if (!rowList.isPlaying()) {
            setDefault();
        } else {
            playing();
        }
    }

    /**
     * cài đặt trạng thái mặc định cho track
     */
    protected void setDefault() {
        rowList.setDefault();
        deleteIntro();
    }

    /**
     * Cài đặt context menu khi click phải track này
     *
     * @param pl playlist nhạc đang được chơi
     * @return context menu đã tạo ra
     */
    private ContextMenu createContextMenu(PlaylistNowPlaying pl) {
        ContextMenu menu = new ContextMenu();
        menu.setId("menu-track");

        MenuItem addPlaylist = new MenuItem("add to playlist");
        addPlaylist.setOnAction((ActionEvent event) -> {
            ContextMenu cmPlaylist = new ContextMenu();
            cmPlaylist.setId("menu-track");

            List<PlaylistUser> allPlaylist = rootPlayer.getPlaylists();
            allPlaylist.forEach((t) -> {
                MenuItem item_playlist = new MenuItem(t.getName());
                item_playlist.setOnAction((event2) -> {
                    List<String> selected = pl.getSourceSelected();
                    if (selected.size() > 0) {
                        if (t.addAll(selected)) {
                            Method.dialog("info", "Ok, Playlist \"" + t.getName() + "\" was updated!");
                        }
                    } else if (t.add(source)) {
                        Method.dialog("info", "Ok, Playlist \"" + t.getName() + "\" was updated!");
                    }

                    pl.unSelectTracks();
                });
                cmPlaylist.getItems().add(item_playlist);
            });
            MenuItem new_playlist = new MenuItem("+ new playlsit");
            new_playlist.setOnAction((event3) -> {
                rootPlayer.createPlaylist(source);
            });
            cmPlaylist.getItems().add(new_playlist);
            cmPlaylist.show(rowList, menu.getX() + 20, menu.getY() + 3);
        });

        MenuItem delete = new MenuItem("delete");
        delete.setOnAction((event) -> {
            System.out.println(">> delete tracks");

            Object res = Method.dialog("confirm", "Do you really want to delete?");
            if (Integer.parseInt(res.toString()) == 0) {
                List<Track> selected = pl.getTrackSelected();
                if (selected.size() > 0) {
                    pl.deleteTracks(selected);
                } else {
                    pl.deleteTrack(this);
                }
            }
        });

        menu.getItems().addAll(addPlaylist, delete);
        return menu;
    }

    /**
     * Tạo bản giới thiệu cho track (hiện thị tên và nghệ sĩ cạnh dưới cover)
     */
    protected void introduceTrack() {
        if (root.getChildren().contains(lblTitle)) {
            return;
        }

        createIntro();
        opaIntro = 0;
        wait = 0;
        lblTitle.setTranslateX(5);
        lblArtist.setTranslateX(5);
        root.getChildren().addAll(lblTitle, lblArtist);
        timer = new javax.swing.Timer(200, null);
        timer.addActionListener((ae) -> {
            if (opaIntro < 1 && wait < 2) {
                opaIntro += 0.05;
                lblTitle.setOpacity(opaIntro);
                lblArtist.setOpacity(opaIntro);
            } else if (++wait > 30 && wait < 51) {
                lblTitle.setTranslateX(wait - 29 + 5);
                lblArtist.setTranslateX(-wait + 29 + 5);
                opaIntro -= 0.05;
                lblTitle.setOpacity(opaIntro);
                lblArtist.setOpacity(opaIntro);
            } else if (++wait > 50 && wait < 60) {
                lblTitle.setVisible(false);
                lblArtist.setVisible(false);
                timer.stop();
            }
        });
        timer.start();
    }

    /**
     * Tạo nội dung cho giới thiệu track
     */
    private void createIntro() {
        lblTitle = new Label("track: " + Method.limitString(title, 45));
        lblArtist = new Label("artist: " + Method.limitString(artist, 45));

        lblTitle.setTranslateY(h / 2 - 100);
        lblArtist.setTranslateY(h / 2 - 70);
        lblTitle.setTranslateX(5);
        lblArtist.setTranslateX(5);
        lblTitle.setId("introTitle");
        lblArtist.setId("introArtist");
        lblTitle.setOpacity(0);
        lblArtist.setOpacity(0);
        lblTitle.setEffect(new Reflection(0, 0.3, 0.4, 0.01));
        lblArtist.setEffect(new Reflection(0, 0.3, 0.4, 0.01));
        lblTitle.setFont(Method.getFont("general", 0));
        lblArtist.setFont(Method.getFont("general", 0));
    }

    /**
     * Xóa đoạn giới thiệu track
     */
    private void deleteIntro() {
        if (root.getChildren().contains(lblArtist)) {
            if (timer.isRunning()) {
                timer.stop();
                timer = null;
            }
            wait = 100;
            root.getChildren().removeAll(lblTitle, lblArtist);
            lblTitle = lblArtist = null;
        }
    }

    /**
     *
     * @return có thể xóa track này hay không
     */
    protected boolean canDelete() {
        return !rowList.isPlaying();
    }

    /**
     * Thực thi việc xóa track này
     *
     * @return có xóa được track hay không
     */
    protected synchronized boolean delete() {
        if (!rowList.isPlaying()) {
            vbox.getChildren().remove(rowList);
            return true;
        } else {
            return false;
        }
    }
}
