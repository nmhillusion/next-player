package nextplayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioSpectrumListener;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

enum TypeLoop {
    NONE, ONE, ALL
}

/**
 * Trung tâm điều khiển mọi thứ trong chương trình
 *
 * @author nmhillusion
 */
public class NextPlayer extends Application {

    private Group root;
    private Tab tabNowPlaying, tabPlaylist;
    private final int width = 800, height = 600, wButton = 50, hButton = 30;
    private PlaylistNowPlaying pl;
    private List<PlaylistUser> listPlaylist;
    private MediaPlayer player;
    private MediaView mv;
    private ProgressPlayer progress;
    private ContextMenu menu;
    private VolumeSlider volumeSlider;
    private double speed = 1, volume, xMove = -1, yMove = -1, screenW, screenH;
    private Stage stage;
    private Label titleStage;
    private ImageView imgCover, boxCover, backgroundPlayer, logo;
    private Rectangle recBackground, clipBG;
    private Reflection EffBoxCover;
    private DropShadow EffTitleStage, dsEffBox;
    private Circle circleCover;
    private Button btnPlay, btnVolume, btnOptions, btnShuffle, btnLoop;
    private GridPane groupControls;
    private Equalizer equalizer;
    private TypeLoop typeLoop = TypeLoop.NONE;
    private VBox lastSessionBox;
    private String lastSessionSong = "<>";
    private double lastSessionTime = 0.0;
    private boolean isDisplayingCover, isPlaying, isLoopOne = false, isClosedCover = false, isShuffle = false, isVideo = true, isFirstPlaying = true;
    private final AudioSpectrumListener audioSpectrumListener = new AudioSpectrumListener() {
        @Override
        public void spectrumDataUpdate(double timestamp, double duration, float[] magnitudes, float[] phases) {
            //  update progress track
            progress.setDuration(getTotalTime());
            progress.setValue(getCurrentTime());

            if (!isVideo) {
                float minVal = player.getAudioSpectrumThreshold();
                //  set effect
                imgCover.setRotate(getCurrentTime() % 90 * 4);
                if (!stage.isFullScreen()) {
                    EffBoxCover.setTopOffset((magnitudes[0] - minVal) / 2);
                    dsEffBox.setRadius((magnitudes[2] - minVal) / 2);
                    EffTitleStage.setRadius(phases[0] * 1.5);
                } else {
                    EffBoxCover.setTopOffset(magnitudes[0] - minVal);
                    dsEffBox.setRadius(magnitudes[2] - minVal);
                    EffTitleStage.setRadius((magnitudes[2] - minVal) / 2);
                }

                //  check end of track
                if (getTotalTime() > 6 && getTotalTime() - getCurrentTime() < 5 && !isClosedCover) {
                    animateCover(false);
                    isClosedCover = true;
                }

                // update equalizer
                equalizer.setValue(magnitudes, minVal);

                //  shake effect
                if (Method.User.isShakeEffect()) {
                    double offset = magnitudes[0] - minVal;
                    if (!stage.isFullScreen()) {
                        backgroundPlayer.setFitWidth(width + offset);
                        backgroundPlayer.setFitHeight(height + offset);
                        backgroundPlayer.setX(-offset / 2);
                        backgroundPlayer.setY(-offset / 2);
                    } else {
                        backgroundPlayer.setFitWidth(screenW + offset);
                        backgroundPlayer.setFitHeight(screenH + offset);
                        backgroundPlayer.setX(-offset / 2);
                        backgroundPlayer.setY(-offset / 2);
                    }
                }
            }
        }
    };

    public static void main(String[] args) {
        launch(NextPlayer.class, args);
        System.out.println("start app: " + args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new Group();
        Scene scene = new Scene(root);
        this.stage = primaryStage;
        
        //  load setting from database
        Method.loadSetting();

        root.setLayoutX(10);
        root.setLayoutY(30);

        addControls();
        progress = new ProgressPlayer(width - 40, 4, height / 2, root, this);
        isPlaying = false;
        volume = 80;
        circleCover = new Circle(102);
        circleCover.setFill(Paint.valueOf("#36f"));
        circleCover.setCenterX(100 + (width / 2 - 200) / 2);
        circleCover.setCenterY(100 + (height / 2 - 200) / 2);

        equalizer = new Equalizer(width, height, root);

        //  setting tabs
        tabNowPlaying = new Tab("Now Playing");
        tabNowPlaying.setClosable(false);
        tabPlaylist = new Tab("Playlist");
        tabPlaylist.setClosable(false);

        TabPane tabPane = new TabPane(tabNowPlaying, tabPlaylist);
        tabPane.setSide(Side.LEFT);
        tabPane.setLayoutX(10);
        tabPane.setLayoutY(progress.getY() + 15);
        tabPane.setPrefSize(width - 20, height - progress.getY() - 20);
        root.getChildren().add(tabPane);

        tabPlaylist.selectedProperty().addListener((observable) -> {
            if (tabPlaylist.isSelected()) {
                // TODO: fixed tab in here
                importPlaylist();
            } else {
                beforeExit();
                listPlaylist = null;
            }
        });
        tabPane.setFocusTraversable(false);

        initPlayer();

        createButton("Exit", false);
        createButton("Minimize", false);
        scene.setFill(Paint.valueOf("transparent"));
        scene.getStylesheets().add("nextPlayer/stylePlayer.css");

        //  solve moving application on the screen
        scene.setOnDragDetected((event) -> {
            xMove = event.getScreenX();
            yMove = event.getScreenY();
            scene.startFullDrag();
        });

        scene.setOnMouseDragOver((event) -> {
            dragMove(event);
        });

        scene.setOnMouseDragExited((event) -> {
            dragMove(event);
        });

        //  solve moving application on the screen (end)

        primaryStage.setWidth(width + 20);
        primaryStage.setHeight(height + 40);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Next Player");
        primaryStage.getIcons().add(Method.ImageFromFile("resource/icon.png"));
        primaryStage.centerOnScreen();
        primaryStage.setResizable(false);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();
    }

    /**
     * Điều khiển việc kéo thả chương trình
     *
     * @param event sự kiện khi người dùng kéo thả
     */
    private void dragMove(MouseDragEvent event) {
        if (xMove == -1) {
            return;
        }
        double xNew = event.getScreenX(), yNew = event.getScreenY();
        stage.setX(stage.getX() + (xNew - xMove));
        stage.setY(stage.getY() + (yNew - yMove));

        xMove = xNew;
        yMove = yNew;
    }

    /**
     * Khởi tạo cho chương trình
     */
    private void initPlayer() {

        //  init class
        PlaylistNowPlaying.init(tabNowPlaying, this);

        Color c = Method.User.getBackgroundColor();
        recBackground = new Rectangle(width + 20, height + 40, Paint.valueOf(Color.hsb(c.getHue(), c.getSaturation(), c.getBrightness(), 0.4).toString()));
        recBackground.setX(-10);
        recBackground.setY(-30);
        recBackground.setArcWidth(12);
        recBackground.setArcHeight(12);
        recBackground.setStroke(Paint.valueOf(c.toString()));
        recBackground.setStrokeWidth(1.5);

        backgroundPlayer = new ImageView(Method.User.getDefaultBackground());
        backgroundPlayer.setFitWidth(width);
        backgroundPlayer.setFitHeight(height);
        backgroundPlayer.setClip(new Rectangle(width, height));
        backgroundPlayer.setEffect(new InnerShadow(50, Color.valueOf("#222")));

        clipBG = new Rectangle(width, height);
        clipBG.setEffect(new DropShadow());

        ImageView iconWin = new ImageView(Method.ImageFromFile("resource/icon.png"));
        iconWin.setX(-5);
        iconWin.setY(-27);
        iconWin.setFitWidth(25);
        iconWin.setFitHeight(25);
        iconWin.setEffect(new DropShadow(3, Color.CORAL));

        titleStage = new Label(":: welcome " + System.getProperty("user.name") + " ::");
        titleStage.setId("titleStage");
        titleStage.setFont(Method.getFont("general", 0));
        titleStage.setAlignment(Pos.CENTER);
        titleStage.setPrefSize(width, 30);
        titleStage.setLayoutY(-30);
        titleStage.setTextFill(Paint.valueOf("#eff"));
        EffTitleStage = new DropShadow(2.5, Color.RED);
        titleStage.setEffect(EffTitleStage);

        root.getChildren().add(0, recBackground);
        root.getChildren().add(1, clipBG);
        root.getChildren().add(2, backgroundPlayer);
        root.getChildren().addAll(iconWin, titleStage);

        root.setOnMouseMoved((event) -> {
            if (stage.isFullScreen() && isVideo) {
                if ((event.getScreenY() > screenH - 200)) {
                    groupControls.setVisible(true);
                    progress.setVisible(true);

                    InnerShadow Eff = new InnerShadow(5, Color.BLACK);
                    Eff.setHeight(400);
                    mv.setEffect(Eff);
                } else {
                    groupControls.setVisible(false);
                    progress.setVisible(false);
                    mv.setEffect(null);
                }
            }
        });

        stage.setOnCloseRequest((event) -> {
            beforeExit();
        });

        introduction();
    }

    /**
     * Giới thiệu trước khi bắt đầu chương trình
     */
    private void introduction() {
        isFirstPlaying = true;
        logo = new ImageView(Method.ImageFromFile("resource/logoNextPlayer.png"));
        logo.setFitWidth(320);
        logo.setFitHeight(120);
        logo.setX(10);
        logo.setY(50);
        root.getChildren().add(logo);

        TranslateTransition t = new TranslateTransition(Duration.seconds(15), logo);
        t.setFromX(10);
        t.setToX(30);
        t.setAutoReverse(true);

        FadeTransition f = new FadeTransition(Duration.seconds(7.5), logo);
        f.setFromValue(0);
        f.setToValue(1);
        f.setAutoReverse(true);

        ParallelTransition p = new ParallelTransition(t, f);
        p.play();
        p.setOnFinished((event) -> {
            p.getChildren().clear();
            p.stop();
        });

        //  load last session
        if (new File("lastSession.nxt").exists()) {
            loadLastSession();
        }
    }

    /**
     * Load lại phiên sử dụng trước đó của người dùng
     */
    private void loadLastSession() {
        try {
            if (new File("lastSession.nxt").canRead()) {
                try (Scanner pen = new Scanner(new File("lastSession.nxt"))) {
                    //  loop
                    if (pen.hasNext()) {
                        int loop = Integer.valueOf(pen.nextLine());
                        if (loop == 1) {
                            actionControl("Loop");
                        } else if (loop == 2) {
                            actionControl("Loop");
                            actionControl("Loop");
                        }
                    }

                    // shuffle
                    if (pen.hasNext() && Boolean.valueOf(pen.nextLine())) {
                        actionControl("Shuffle");
                    }
                    String nowSong = "", srcImg = "";
                    short n = 0;

                    //  volume
                    if (pen.hasNext()) {
                        volume = Double.valueOf(pen.nextLine());
                    }

                    //  last song
                    if (pen.hasNext()) {
                        lastSessionSong = pen.nextLine();
                    }
                    //  last time
                    if (pen.hasNext()) {
                        lastSessionTime = Double.valueOf(pen.nextLine());
                    }
                    //  background
                    if (pen.hasNext()) {
                        srcImg = pen.nextLine();
                    }
                    //  tracks
                    List<File> list = new LinkedList();
                    while (pen.hasNext()) {
                        String path = pen.nextLine();
                        list.add(new File(URI.create(path)));
                    }
                    if (list.size() > 0) {
                        pl = new PlaylistNowPlaying(list, Method.ImageFromFile(srcImg), isShuffle, width, height, root);
                        pl.setBackground(srcImg);
                        pl.setLoop(typeLoop == TypeLoop.ALL);
                    }
                    if (lastSessionBox != null) {
                        root.getChildren().remove(lastSessionBox);
                        lastSessionBox = null;
                    }
                }
            }
        } catch (FileNotFoundException ex) {
            Method.dialog("alert", "Can not save this session.");
        }
    }

    /**
     * Bắt đầu play khi load xong các track
     */
    protected void startPlaying() {
        if (isFirstPlaying) {
            for (Track t : pl.getListTrack()) {
                if (t.getSource().compareTo(lastSessionSong) == 0) {
                    pl.seekTrack(t);
                    player.setOnReady(() -> {
                        if (lastSessionTime > 0) {
                            player.seek(Duration.seconds(lastSessionTime));
                            pause();
                            t.updateMetadata("duration", player.getTotalDuration().toSeconds());
                            lastSessionTime = -1;
                            System.out.println("is first playing!");
                        }
                    });
                    break;
                }
            }
        } else {
            play();
            System.out.println("not first playing!");
        }
    }

    /**
     * Cài đặt hình nền cho chương trình
     *
     * @param source hình nền muốn đặt
     */
    protected void setBackground(Image source) {
        backgroundPlayer.setImage(source);
    }

    /**
     * Cập nhật lại chương trình sau khi người dùng cài đặt
     */
    protected void updateSetting() {
        setBackground(Method.User.getDefaultBackground());

        Color c = Method.User.getBackgroundColor();
        recBackground.setFill(Paint.valueOf(Color.hsb(c.getHue(), c.getSaturation(), c.getBrightness(), 0.4).toString()));
        recBackground.setStroke(Paint.valueOf(c.toString()));

        equalizer.updateBarColor();
        progress.updateColor();
    }

    /**
     * Các công việc trước khi thoát chương trình: - Lưu lại các playlist - Lưu
     * lại phiên sử dụng hiện tại
     */
    private void beforeExit() {
        if (listPlaylist != null) {
            listPlaylist.forEach((t) -> {
                t.savePlaylist();
            });
        }

        if (isPlayer()) {
            try {
                try (PrintWriter pen = new PrintWriter(new File("lastSession.nxt"))) {
                    pen.println((typeLoop == TypeLoop.ONE) ? 1 : (typeLoop == TypeLoop.ALL) ? 2 : 0);
                    pen.println(isShuffle);
                    pen.println(volume);
                    if (isPlayer()) {
                        pen.println(pl.nowPlay().getSource());
                        pen.println(player.getCurrentTime().toSeconds());
                        pen.println(pl.getBackground());

                        pl.getListTrack().forEach((t) -> {
                            pen.println(t.getSource());
                        });
                    }
                }
            } catch (FileNotFoundException ex) {
                Method.dialog("alert", "Can not save this session.");
            }
        }
    }

    /**
     * Thêm vào các nút điều khiển
     */
    private void addControls() {
        groupControls = new GridPane();
        groupControls.setHgap(4);
        groupControls.setVgap(8);
        groupControls.setAlignment(Pos.CENTER);
        groupControls.setGridLinesVisible(false);
        groupControls.setLayoutX(width / 2 + 15);
        groupControls.setLayoutY(height / 3 - 10);

        Button ibtnOps = createButton("Options", true),
                ibtnFolder = createButton("Folder", true),
                ibtnPlay = createButton("Play", true),
                ibtnStop = createButton("Stop", true),
                ibtnPrevious = createButton("Previous", true),
                ibtnNext = createButton("Next", true),
                ibtnShuffle = createButton("Shuffle", true),
                ibtnLoop = createButton("Loop", true),
                ibtnVolume = createButton("Volume", true); // finally
        volumeSlider = new VolumeSlider(width / 2 - 2 * wButton - 70, hButton / 4, this);

        groupControls.addRow(0, ibtnOps, ibtnFolder, ibtnPlay, ibtnStop, ibtnPrevious, ibtnNext, ibtnShuffle);
        groupControls.addRow(1, ibtnLoop, ibtnVolume);
        groupControls.add(volumeSlider, 2, 1, 5, 1);

        createButton("FullScreen", false);
        root.getChildren().add(groupControls);
    }

    /**
     * Tạo ra nút điều khiển
     *
     * @param title tiêu đề của nút
     * @param isControl có phải là nút thuộc khung điều khiển không
     */
    private Button createButton(String title, boolean isControl) {
        Button btn = new Button();
        btn.setId("btn" + title);
        btn.setTooltip(new Tooltip(title.compareTo("Folder") != 0 ? title : "Open File(s)"));
        btn.setPrefSize(wButton, hButton);
        btn.setFocusTraversable(false);

        if (title.compareTo("FullScreen") == 0) {
            btn.setLayoutX(width / 2 - wButton - 20);
            btn.setLayoutY(height / 3 + hButton);
        }

        DropShadow btnEff = new DropShadow();
        if (title.compareTo("Exit") == 0 || title.compareTo("Minimize") == 0) {
            int x;
            if (title.compareTo("Exit") == 0) {
                x = width + 10 - wButton - 2;     // -2 for width of border
            } else {
                x = width + 10 - 2 * wButton - 2;
            }
            int y = -hButton + 1;
            btn.setLayoutX(x);
            btn.setLayoutY(y);

            btn.setText(title.compareTo("Exit") == 0 ? "x" : "-");

            btn.setOnMouseMoved((event) -> {
                if (title.compareTo("Exit") != 0) {
                    btn.setEffect(new DropShadow(10, Color.valueOf("#359bed")));
                } else {
                    btn.setEffect(new DropShadow(10, Color.valueOf("#f00")));
                }
            });
            btn.setOnMouseExited((event) -> {
                btn.setEffect(btnEff);
            });
        } else {
            btn.setOnMouseMoved((event) -> {
                btn.setEffect(new DropShadow(10, 0, 0, Color.valueOf("#359bed")));
            });
            btn.setOnMouseExited((event) -> {
                btn.setEffect(btnEff);
            });
        }

        javafx.scene.layout.BackgroundImage bg = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btn" + title + ".png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.SPACE, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
        btn.setBackground(new javafx.scene.layout.Background(bg));
        btn.setEffect(btnEff);

        switch (title) {
            case "Play":
                btnPlay = btn;
                break;
            case "Volume":
                btnVolume = btn;
                break;
            case "Options":
                btnOptions = btn;
                break;
            case "Shuffle":
                btnShuffle = btn;
                break;
            case "Loop":
                btnLoop = btn;
                break;
        }

        if (!isControl) {
            root.getChildren().add(btn);
        }

        /**
         * điều khiển sự kiện khi người dùng nhấn bàn phím cho các nút điều
         * khiển
         */
        root.setOnKeyReleased((event) -> {
            String action = null;
            switch (event.getCode()) {
                case U:
                    if (event.isControlDown()) {
                        action = "Options";
                    }
                    break;

                case O:
                    if (event.isControlDown()) {
                        action = "Folder";
                    }
                    break;

                case SPACE:
                    if (isPlayer()) {
                        action = "Play";
                    }
                    break;

                case LEFT:
                    if (event.isControlDown()) {
                        action = "Previous";
                    } else {
                        seek(getCurrentTime() - Method.User.getTimeSkip());
                    }
                    break;

                case RIGHT:
                    if (event.isControlDown()) {
                        action = "Next";
                    } else {
                        seek(getCurrentTime() + Method.User.getTimeSkip());
                    }
                    break;

                case M:
                    if (event.isControlDown()) {
                        action = "Volume";
                    }
                    break;

                case F:
                    action = "FullScreen";
                    break;

                case UP:
                    if (isPlayer() && event.isControlDown()) {
                        volume += 10;
                        if (!setVolume(volume)) {
                            volume = 99;
                        }
                    }
                    break;

                case DOWN:
                    if (isPlayer() && event.isControlDown()) {
                        volume -= 10;
                        if (!setVolume(volume)) {
                            volume = 0;
                        }
                    }
                    break;
            }

            if (action != null) {
                actionControl(action);
            }
        });

        btn.setOnAction((ActionEvent event) -> {
            actionControl(title);
        });

        return btn;
    }

    /**
     * thực thi hành động với các nút điều khiển tương ứng
     *
     * @param title tiêu đề của nút điều khiển tương ứng
     */
    private void actionControl(String title) {
        switch (title) {
            case "Options":
                Method.createContextMenuSetting(btnOptions, (short) (groupControls.getLayoutX() + 20 + stage.getX()),
                        (short) (groupControls.getLayoutY() + stage.getY()), this);
                break;
            case "Folder":
                loadMusics(chooseFiles(), typeLoop == TypeLoop.ALL);
                break;
            case "Play":
                if (!(pl instanceof PlaylistNowPlaying)) {
                    actionControl("Folder");
                    break;
                }
                if (isPlaying) {
                    pause();
                } else {
                    play();
                }
                break;
            case "Stop":
                istop();
                break;
            case "Next":
                next();
                break;
            case "Previous":
                previous();
                break;
            case "Shuffle":
                if (isShuffle) {
                    isShuffle = false;
                    if (pl instanceof PlaylistNowPlaying) {
                        pl.setShuffle(false);
                    }
                    javafx.scene.layout.BackgroundImage bg1 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnShuffle.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnShuffle.setBackground(new javafx.scene.layout.Background(bg1));
                } else {
                    isShuffle = true;
                    if (pl instanceof PlaylistNowPlaying) {
                        pl.setShuffle(true);
                    }
                    javafx.scene.layout.BackgroundImage bg2 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnShuffleOn.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnShuffle.setBackground(new javafx.scene.layout.Background(bg2));
                }
                break;
            case "Loop":
                switch (typeLoop) {
                    case NONE:
                        typeLoop = TypeLoop.ONE;
                        isLoopOne = true;
                        if (pl instanceof PlaylistNowPlaying) {
                            pl.setLoop(false);
                        }
                        break;

                    case ONE:
                        typeLoop = TypeLoop.ALL;
                        isLoopOne = false;
                        if (pl instanceof PlaylistNowPlaying) {
                            pl.setLoop(true);
                        }
                        break;

                    case ALL:
                        typeLoop = TypeLoop.NONE;
                        isLoopOne = false;
                        if (pl instanceof PlaylistNowPlaying) {
                            pl.setLoop(false);
                        }
                        break;

                    default:
                        Method.dialog("alert", "// error in set loop player");
                        break;
                }
                if (typeLoop != TypeLoop.NONE) {
                    javafx.scene.layout.BackgroundImage bg3 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnLoop" + typeLoop.name() + ".png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnLoop.setBackground(new javafx.scene.layout.Background(bg3));
                } else {
                    javafx.scene.layout.BackgroundImage bg4 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnLoop.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnLoop.setBackground(new javafx.scene.layout.Background(bg4));
                }
                break;
            case "Volume":
                if (!isPlayer()) {
                    break;
                }
                if (player.isMute()) {
                    player.setMute(false);
                    javafx.scene.layout.BackgroundImage bg5 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnVolume.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnVolume.setBackground(new javafx.scene.layout.Background(bg5));
                } else {
                    player.setMute(true);
                    javafx.scene.layout.BackgroundImage bg6 = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnMute.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
                    btnVolume.setBackground(new javafx.scene.layout.Background(bg6));
                }
                break;
            case "FullScreen":
                if (isPlayer()) {
                    if (!stage.isFullScreen()) {
                        fullScreen();
                    } else {
                        exitFullScreen();
                    }
                }
                break;
            case "Exit":
                beforeExit();
                System.exit(0);
                break;
            case "Minimize":
                stage.setIconified(true);
                break;
            default:
                Method.dialog("alert", "Error! Button is not available!");
                break;
        }
    }

    /**
     * Nhập vào các playlist từ file thư mục (chưa hoàn thiện - chưa xử lý
     * trường hợp người dùng cố tình xóa thư mục playlist)
     */
    private void importPlaylist() {
        PlaylistUser.reset();

        Scanner in;
        File folderPlaylist;

        folderPlaylist = new File("playlist");
        if (!folderPlaylist.isDirectory()) {
            Method.dialog("alert", "Can not find \"playplist\" folder!");
            return;
        }

        listPlaylist = new LinkedList<>();
        File[] listFile = folderPlaylist.listFiles();
        for (short i = 0; i < listFile.length; ++i) {
            try {
                if (listFile[i].getAbsolutePath().lastIndexOf(".m3u") != listFile[i].getAbsolutePath().length() - 4) {
                    continue;   //  this file is not playlist
                }
                in = new Scanner(listFile[i]);
                String fileName = listFile[i].getName();
                PlaylistUser newPlaylist = new PlaylistUser(fileName.substring(0, fileName.lastIndexOf(".")),
                        in.nextLine(), width, tabPlaylist, this);
                listPlaylist.add(newPlaylist);
                while (in.hasNext()) {
                    newPlaylist.add(in.nextLine());
                }
                //  add to tab
                newPlaylist.addToTabPane();
            } catch (FileNotFoundException ex) {
                Method.dialog("alert", "// error: can not read playlist: " + listFile[i].getName());
            }
        }
    }

    /**
     * Lấy về tất cả danh sách của người dùng
     *
     * @return danh sách người dùng
     */
    protected List<PlaylistUser> getPlaylists() {
        importPlaylist();
        return listPlaylist;
    }

    /**
     * Người dùng tạo ra một danh sách
     *
     * @param source track nơi mà người dùng nhấp chuột phải khi tạo
     * @return danh sách người dùng khi tạo ra thành công
     */
    protected PlaylistUser createPlaylist(String source) {
        String name = Method.dialog("prompt", "input name of playlist: ").toString();
        boolean exist = false;
        importPlaylist();

        for (PlaylistUser t : listPlaylist) {
            if (t.getName().compareTo(name) == 0) {
                Method.dialog("alert", "// playlist's name is existed, please input other name!");
                exist = true;
                break;
            }
        }

        if (!exist) {
            PlaylistUser newPlaylist = new PlaylistUser(name, width, tabPlaylist, this);
            List<String> arrSource = pl.getSourceSelected();
            if (arrSource.size() > 0) {
                newPlaylist.addAll(arrSource);
            } else {
                newPlaylist.add(source);
            }
            listPlaylist.add(newPlaylist);
            Method.dialog("info", "Ok, Playlist \"" + name + "\" was created!");
            pl.unSelectTracks();
            newPlaylist.savePlaylist();

            return newPlaylist;
        } else {
            createPlaylist(source);
            return null;
        }
    }

    /**
     * Người dùng xóa đi một playlist
     *
     * @param p playlist mà người dùng muốn xóa
     */
    protected void deletePlaylist(PlaylistUser p) {
        if (listPlaylist.remove(p)) {
            File f = new File("playlist/" + p.getName() + "-deleted.txt");
            if (!new File("playlist/" + p.getName() + ".m3u").renameTo(f)) {
                Method.dialog("alert", "Can not delete bacause this playlist is opening by other program.");
            } else {
                p.deleteDisplay(null, listPlaylist.indexOf(p));
                p.numbericList();
                Method.dialog("info", p.getName() + " playlist was deleted!");
                p = null;
            }
            f.deleteOnExit();
        } else {
            Method.dialog("alert", p.getName() + " can not be deleted!");
        }
        System.gc();
    }

    /**
     * Người dùng chọn các file từ ổ đĩa để chơi nhạc
     *
     * @return danh sách các file mà người dùng đã chọn
     */
    private List<File> chooseFiles() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open File(s):");
        FileChooser.ExtensionFilter allSupport = new FileChooser.ExtensionFilter("All file support", "*.mp3", "*.mp4");
        fc.getExtensionFilters().addAll(allSupport);
        fc.setInitialDirectory(new File("\\"));
        return fc.showOpenMultipleDialog(stage);
    }

    /**
     * Người dùng chọn chạy một danh sách nhạc
     *
     * @param list danh sách các bài hát của playlist mà người dùng đã chọn
     * @param srcImg đường dẫn đến file ảnh nền
     */
    protected void playPlaylist(List<String> list, String srcImg) {
        List<File> songs = new LinkedList<>();
        list.forEach((t) -> {
            songs.add(new File(URI.create(t)));
        });

        pl = new PlaylistNowPlaying(songs, Method.ImageFromFile(srcImg), isShuffle, width, height, root);
        pl.setBackground(srcImg);
        pl.setLoop(typeLoop == TypeLoop.ALL);
    }

    /**
     * Thực hiện load các bài hát vào playlist đang chơi sau khi đã chọn từ ổ
     * đĩa
     *
     * @param list danh sách các file mà người dùng đã chọn
     * @param loop có lặp playlist này không
     */
    private void loadMusics(List<File> list, boolean loop) {
        if (list != null) {
            if (isPlayer()) {
                istop();
            }
            pl = new PlaylistNowPlaying(list, Method.User.getDefaultBackground(), isShuffle, width, height, root);
            pl.setLoop(loop);
        }
    }

    /**
     * Thực hiện load một bài hát trước khi có thể chạy
     *
     * @return có load được hay không
     */
    private synchronized boolean loadMusic() {
        if (player instanceof MediaPlayer && player.getStatus() == MediaPlayer.Status.PAUSED) {
            return true;
        } else if (player instanceof MediaPlayer && player.getStatus() != MediaPlayer.Status.DISPOSED
                && player.getStatus() != MediaPlayer.Status.UNKNOWN) {
            player.dispose();
        }

        stage.setTitle("Next Player");
        titleStage.setText(":: Next Player ::");
        root.getChildren().removeAll(imgCover, boxCover, circleCover, mv);
        progress.setPreview(null);

        if (pl == null || pl.nowPlay() == null) {
            return false;
        }

        Track track = pl.nowPlay();
        player = new MediaPlayer(track.getMedia());
        setVolume(volume);
        player.setOnEndOfMedia(() -> {
            next();
        });
        player.setOnError(() -> {
            Method.dialog("alert", "MediaPlayer: Error when loading file!");
        });

        player.setOnReady(() -> {
            ObservableMap<String, Object> metadata = player.getMedia().getMetadata();
            metadata.forEach((key, value) -> {
                track.updateMetadata(key, value);
            });

            track.updateMetadata("duration", player.getTotalDuration().toSeconds());

            if (isVideo) {
                float sW = width / 2, sH = sW * 9 / 16,
                        oW = player.getMedia().getWidth(), oH = player.getMedia().getHeight(),
                        nW = sH * oW / oH,
                        offX = (sW - nW) / 2;
                mv.setX(offX);
                mv.setEffect(new DropShadow(3, Color.CORAL));
            }
        });

        player.volumeProperty().addListener((observable) -> {
            volumeSlider.setValue(player.getVolume() * 100, true);
        });

        stage.setTitle("Next Player | " + track.getTitle());
        titleStage.setText(":: " + Method.limitString(track.getTitle(), 60) + " - " + Method.limitString(track.getArtist(), 15) + " ::");
        String newColor = Method.getRandomColor();
        EffTitleStage.setColor(Color.valueOf(newColor));

        if (track.isVideo()) {
            equalizer.setVisible(false);

            mv = new MediaView(player);
            mv.setSmooth(true);
            mv.setFitWidth(width / 2);
            mv.setFitHeight((width / 2) * 9 / 16);
            mv.setY(0);
            root.getChildren().add(mv);
            for (Node t : root.getChildren()) {
                if (t instanceof Button && ((Button) t).getTooltip().getText().compareTo("FullScreen") == 0) {
                    t.toFront();
                    break;
                }
            }
            progress.toFront();

            if (Method.User.isPreviewVideo()) {
                progress.setPreview(new MediaView(new MediaPlayer(track.getMedia())));
            }

            isVideo = true;
        } else {
            isVideo = false;
            equalizer.setVisible(true);

            if (isPlayer()) {
                Image cover = track.getCover();

                if (cover == null) {
                    imgCover = new ImageView(Method.User.getDefaultCoverImg());
                } else {
                    imgCover = new ImageView(cover);
                }

                imgCover.setId("imgCover");
                imgCover.setSmooth(true);
//                TODO: reflection cover
//                imgCover.setEffect(new Reflection());

                Circle clip = new Circle(100);
                clip.setCenterX(100);
                clip.setCenterY(100);
                imgCover.setClip(clip);

                boxCover = new ImageView(imgCover.getImage());
                boxCover.setLayoutX((width / 2 - 204) / 2);
                boxCover.setLayoutY((height / 2 - 204) / 2);
                boxCover.setFitWidth(204);
                boxCover.setFitHeight(204);

                dsEffBox = new DropShadow(5, Color.valueOf(newColor));
                EffBoxCover = new Reflection(0, 0.4, 0.4, 0.01);
                EffBoxCover.setInput(dsEffBox);
//                boxCover.setEffect(EffBoxCover);

                imgCover.setFitWidth(200);
                imgCover.setFitHeight(200);
                imgCover.setLayoutX((width / 2 - 200) / 2);
                imgCover.setLayoutY((height / 2 - 200) / 2);

                root.getChildren().addAll(boxCover, circleCover, imgCover);
            }
        }
        track.introduceTrack();
        System.gc();

        tabNowPlaying.getTabPane().getSelectionModel().select(tabNowPlaying);
        return true;
    }

    /**
     * Hiệu ứng cho Cover khi bắt đầu và kết thúc bài hát
     *
     * @param fadeIn có phải là bắt đầu bài hát không
     */
    private void animateCover(boolean fadeIn) {
        Node cover[] = {imgCover, circleCover, boxCover};
        for (Node obj : cover) {
            if (fadeIn) {
                ScaleTransition tEff = new ScaleTransition(Duration.seconds(3), obj);
                tEff.setFromX(0);
                tEff.setFromY(0);
                tEff.setToX(1);
                tEff.setToY(1);
                tEff.setCycleCount(1);

                tEff.play();
                tEff.setOnFinished((event) -> {
                    isDisplayingCover = true;
                });
            } else {
                ScaleTransition tEff = new ScaleTransition(Duration.seconds(3), obj);
                tEff.setFromX(1);
                tEff.setFromY(1);
                tEff.setToX(0);
                tEff.setToY(0);
                tEff.setCycleCount(1);

                tEff.play();
                tEff.setOnFinished((event) -> {
                    isDisplayingCover = false;
                });
            }
        }
    }

    /**
     * Các công việc cần làm khi người dùng đang chơi nhạc
     */
    protected synchronized void play() {
        System.gc();
        System.out.println("pl ready: " + pl.isReady());
        if (pl.isReady() && loadMusic() && player.getStatus() != MediaPlayer.Status.PLAYING) {
            if (isFirstPlaying) {
                isFirstPlaying = false;
                root.getChildren().removeAll(logo, lastSessionBox);
                logo = null;
                lastSessionBox = null;
            }

            player.play();

            isPlaying = true;
            isClosedCover = false;
            if (!isVideo && !isDisplayingCover) {
                animateCover(true);
            }

            javafx.scene.layout.BackgroundImage bg = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnPause.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
            btnPlay.setBackground(new javafx.scene.layout.Background(bg));
            btnPlay.setTooltip(new Tooltip("Pause"));

            if (stage.isFullScreen()) {
                fullScreen();
            }

            if (player.getAudioSpectrumListener() == null) {
                player.setAudioSpectrumListener(audioSpectrumListener);
            }
        }
    }

    public boolean isFirstPlaying() {
        return isFirstPlaying;
    }

    /**
     *
     * @return hiện tại nó có phải là player không, có load bài hát và chơi nhạc
     * được chưa
     */
    protected boolean isPlayer() {
        return pl instanceof PlaylistNowPlaying && player instanceof MediaPlayer;
    }

    /**
     * Tạm dừng player
     */
    void pause() {
        javafx.scene.layout.BackgroundImage bg = new javafx.scene.layout.BackgroundImage(Method.ImageFromFile("resource/btnPlay.png"), javafx.scene.layout.BackgroundRepeat.ROUND, javafx.scene.layout.BackgroundRepeat.NO_REPEAT, javafx.scene.layout.BackgroundPosition.CENTER, javafx.scene.layout.BackgroundSize.DEFAULT);
        btnPlay.setBackground(new javafx.scene.layout.Background(bg));
        btnPlay.setTooltip(new Tooltip("Play"));
        if (isPlayer()) {
            player.pause();
            isPlaying = false;
            System.gc();
        }
    }

    /**
     * Nhảy sang bài tiếp theo
     */
    private void next() {
        if (isPlayer()) {
            player.stop();
            if (isLoopOne) {
                play();
            } else if (pl.next() != null) {
                play();
            }
        }
    }

    /**
     * Nhảy lại bài trước đó
     */
    private void previous() {
        if (isPlayer()) {
            player.stop();
            if (isLoopOne) {
                play();
            } else if (pl.previous() != null) {
                play();
            }
        }
    }

    /**
     * dừng player (phải là istop để tranh trùng stop của thread)
     */
    private synchronized void istop() {
        if (isPlayer()) {
            pause();
            progress.setValue(Double.MAX_VALUE);
            player.stop();
        }
    }

    /**
     * Cài đặt âm lượng cho player
     *
     * @param v giá trị âm lượng muốn cài
     * @return có cài âm lượng được không
     */
    protected boolean setVolume(double v) {
        if (v > 0 && v <= 100) {
            volume = v;
            if (isPlayer()) {
                player.setVolume(v / 100);
            }
            volumeSlider.setValue(v, true);
            return true;
        }
        return false;
    }

    /**
     * player nhảy đến một khoảng thời gian nào đó của track
     *
     * @param step thời gian muốn nhảy đến
     */
    protected void seek(double step) {
        if (isPlayer() && step > 0 && step <= getTotalTime()) {
            player.seek(Duration.seconds(step));
            if (getTotalTime() - getCurrentTime() > 5 && !isDisplayingCover) {
                animateCover(true);
                isClosedCover = false;
            }
        }
    }

    /**
     * player nhảy đến một khoảng thời gian nào đó của track
     *
     * @param step thời gian muốn nhảy đến
     */
    protected void seek(String time) {
        if (isPlayer()) {
            player.seek(Duration.valueOf(time));
        }
    }

    /**
     *
     * @return thời gian hiện tại của player
     */
    private double getCurrentTime() {
        if (isPlayer()) {
            return player.getCurrentTime().toSeconds();
        } else {
            return 0;
        }
    }

    /**
     * Tổng thời gian của track hiện tại
     *
     * @return
     */
    private int getTotalTime() {
        if (isPlayer()) {
            return Math.round(Math.round(player.getMedia().getDuration().toSeconds()));
        } else {
            return 0;
        }
    }

    /**
     * Tăng tốc độ play của player
     */
    private void speedUp() {
        if (speed < 8) {
            speed += 0.5;
            player.setRate(speed);
        }
    }

    /**
     * Giảm tốc độ player của player
     */
    private void speedDown() {
        if (speed > 0.5) {
            speed -= 0.5;
            player.setRate(speed);
        }
    }

    /**
     * Cài đặt cho chế độ toàn màn hình
     */
    private synchronized void fullScreen() {
        root.setLayoutX(0);
        root.setLayoutY(0);

        menu = new ContextMenu();
        menu.setId("menu-fullscreen");

        stage.setFullScreen(true);
        screenW = stage.getWidth();
        screenH = stage.getHeight();
        if (pl.nowPlay().isVideo()) {     //  is video
            mv.setFitWidth(stage.getWidth());
            mv.setFitHeight(stage.getHeight());
            mv.toFront();
            mv.setOnContextMenuRequested((event) -> {
                if (stage.isFullScreen()) {
                    menu.show(mv, event.getX(), event.getY());
                }
            });
            mv.setOnMouseReleased((eventDel) -> {
                if (menu instanceof ContextMenu) {
                    menu.hide();
                }
            });
        } else {              //  is music            
            //  set background
            backgroundPlayer.setFitWidth(screenW);
            backgroundPlayer.setFitHeight(screenH);
            clipBG.setWidth(screenW);
            clipBG.setHeight(screenH);
            backgroundPlayer.setClip(new Rectangle(screenW, screenH));
            clipBG.toFront();
            backgroundPlayer.toFront();

            backgroundPlayer.setOnContextMenuRequested((event) -> {
                if (stage.isFullScreen()) {
                    menu.show(backgroundPlayer, event.getX(), event.getY());
                }
            });
            backgroundPlayer.setOnMouseReleased((eventDel) -> {
                if (menu instanceof ContextMenu) {
                    menu.hide();
                }
            });

            setFullScreenCover();

            equalizer.setFullscreen(screenW, screenH);

            titleStage.setLayoutX((screenW - width) / 2);
            titleStage.setLayoutY(screenH - 160);
            titleStage.setId("titleStage-fullscreen");
            titleStage.toFront();
        }

        //  context menu        
        menu.getItems().add(new SeparatorMenuItem());

        MenuItem btnSpeedUp = new MenuItem(" Speed Up"),
                btnSpeedDown = new MenuItem(" Speed Down");

        menu.getItems().add(btnSpeedUp);
        btnSpeedUp.setOnAction((event) -> {
            speedUp();
            if (speed == 8) {
                btnSpeedUp.setDisable(true);
            } else {
                btnSpeedUp.setDisable(false);
            }

            if (btnSpeedDown.isDisable()) {
                btnSpeedDown.setDisable(false);
            }
        });

        menu.getItems().add(btnSpeedDown);
        btnSpeedDown.setOnAction((event) -> {
            speedDown();
            if (speed == 0.5) {
                btnSpeedDown.setDisable(true);
            } else {
                btnSpeedDown.setDisable(false);
            }

            if (btnSpeedUp.isDisable()) {
                btnSpeedUp.setDisable(false);
            }
        });

        menu.getItems().add(new SeparatorMenuItem());

        MenuItem btnExitFullScreen = new MenuItem(" Exit Fullscreen (ESC) ");
        btnExitFullScreen.setStyle("-fx-font-weight: BOLD;");
        menu.getItems().add(btnExitFullScreen);

        menu.getItems().add(new SeparatorMenuItem());

        MenuItem info = new MenuItem(" next player @nmhillusion ");
        info.setStyle("-fx-text-fill: #36f;");
        info.setDisable(true);
        menu.getItems().add(info);

        menu.getItems().add(new SeparatorMenuItem());

        btnExitFullScreen.setOnAction((eventExit) -> {
            exitFullScreen();
        });
        //  context menu (end)

        root.setOnKeyPressed((event) -> {
            if ((event.getCode() == KeyCode.ESCAPE || event.getCode() == KeyCode.F) && !stage.isFullScreen()) {
                exitFullScreen();
            }
        });

//          grounp controls
        groupControls.setPrefWidth(screenW);
        groupControls.setAlignment(Pos.CENTER);
        groupControls.setLayoutX(0);
        groupControls.setLayoutY(screenH - 100);
        groupControls.toFront();

        progress.setFullScreen(screenW, screenH);
    }

    /**
     * Cài đặt chế độ toàn màn hình cho cover
     */
    private void setFullScreenCover() {
        if (isVideo) {
            return;
        }
        byte x = 2;
//            boxCover
        boxCover.setLayoutX(x * (screenW / 2 - 204) / 2);
        boxCover.setLayoutY(x * (height / 2 - 204) / 2);
        boxCover.setFitWidth(x * 204);
        boxCover.setFitHeight(x * 204);
        boxCover.toFront();
//            circleCover
        circleCover.setRadius(x * 102);
        circleCover.setCenterX(x * (100 + (screenW / 2 - 200) / 2));
        circleCover.setCenterY(x * (100 + (height / 2 - 200) / 2));
        circleCover.toFront();

//            imgCover
        imgCover.setFitWidth(200 * x);
        imgCover.setFitHeight(200 * x);
        imgCover.setLayoutX(x * (screenW / 2 - 200) / 2);
        imgCover.setLayoutY(x * (height / 2 - 200) / 2);
        imgCover.toFront();

        Circle clip = new Circle(x * 100);
        clip.setCenterX(x * 100);
        clip.setCenterY(x * 100);
        imgCover.setClip(clip);
    }

    /**
     * Thoát khỏi chế độ toàn màn hình
     */
    private void exitFullScreen() {
        if (stage.isFullScreen()) {
            stage.setFullScreen(false);
        }
        progress.exitFullScreen();
        progress.setVisible(true);

        //          grounp controls
        groupControls.setAlignment(Pos.BASELINE_LEFT);
        groupControls.setLayoutX(width / 2 + 25);
        groupControls.setLayoutY(height / 3 - 10);
        groupControls.toBack();
        groupControls.setVisible(true);
        groupControls.setPrefWidth(width / 2);

        if (pl.nowPlay().isVideo()) {
            mv.setEffect(new DropShadow(3, Color.CORAL));
            mv.setFitWidth(width / 2);
            mv.setFitHeight(height / 2);
            mv.toBack();
        } else {
//            imgCover
            imgCover.setFitWidth(200);
            imgCover.setFitHeight(200);
            imgCover.setLayoutX((width / 2 - 200) / 2);
            imgCover.setLayoutY((height / 2 - 200) / 2);
            imgCover.toBack();

            Circle clip = new Circle(100);
            clip.setCenterX(100);
            clip.setCenterY(100);
            imgCover.setClip(clip);
//            circleCover
            circleCover.setRadius(102);
            circleCover.setCenterX(100 + (width / 2 - 200) / 2);
            circleCover.setCenterY(100 + (height / 2 - 200) / 2);
            circleCover.toBack();

//            boxCover
            boxCover.setLayoutX((width / 2 - 204) / 2);
            boxCover.setLayoutY((height / 2 - 204) / 2);
            boxCover.setFitWidth(204);
            boxCover.setFitHeight(204);
            boxCover.toBack();

            equalizer.exitFullscreen();

            titleStage.setLayoutX(0);
            titleStage.setLayoutY(-30);
            titleStage.setId("titleStage");
            titleStage.toBack();

            backgroundPlayer.setFitWidth(width);
            backgroundPlayer.setFitHeight(height);
            clipBG.setWidth(width);
            clipBG.setHeight(height);
            backgroundPlayer.setClip(new Rectangle(width, height));
        }

        backgroundPlayer.toBack();
        clipBG.toBack();
        recBackground.toBack();
        menu = null;
        System.gc();
        root.setLayoutX(10);
        root.setLayoutY(30);
    }
}
