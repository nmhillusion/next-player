package nextplayer;

import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.input.MouseEvent;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

/**
 *  Thanh tiến trình chơi nhạc của player
 * @author nmhillusion
 */
final class ProgressPlayer{
    private Rectangle bar, value;
    private double max, y, dur, defaultW;
    private Group root;
    private MediaView preview = null;
    private Label tooltip, time;
    private boolean isFullScreen = false;
    
    /**
     * Khởi tạo cho thanh tiến trình chơi nhạc
     * @param w chiều rộng ứng dụng
     * @param h chiều cao ứng dụng
     * @param y tọa độ y của thanh tiến trình
     * @param root ổ chứa chính của chương trình
     * @param player thể hiện của chương trình chính
     */
    public ProgressPlayer(double w, double h, double y, Group root, NextPlayer player){
        bar = new Rectangle(w, h, Paint.valueOf("#111"));
        value = new Rectangle(0, h, Method.User.getProgressColor());
        defaultW = max = w;
        dur = 0;
        this.root = root;
        this.y = y;
        
        bar.setEffect(new DropShadow(3, Method.User.getProgressColor()));
        value.setEffect(new DropShadow(1, Method.User.getProgressColor()));
        bar.setY(y); value.setY(y);
        bar.setArcHeight(h);bar.setArcWidth(h);
        value.setArcHeight(h);value.setArcWidth(h);
        bar.setCursor(Cursor.HAND); value.setCursor(Cursor.HAND);
        bar.setTranslateX(20);value.setTranslateX(20);
        
        tooltip = new Label();
        tooltip.setTextFill(Paint.valueOf("#eff"));
        tooltip.setId("tooltip-custom");
        tooltip.setFont(Method.getFont("time",0));
        tooltip.setPrefWidth(60);
        tooltip.setAlignment(Pos.CENTER);
        tooltip.setVisible(false);
        
        time = new Label();
        time.setTextFill(Method.User.getProgressColor());
        time.setId("time-custom");
        time.setFont(Method.getFont("time",0));
        time.autosize();
        time.setTranslateX(0);time.setTranslateY(y - 20);
        time.setEffect(new Reflection(0,0.5, 0.5, 0.1));
        time.setVisible(false);
        root.getChildren().addAll(time, bar, value, tooltip);
        
        time.setOnMouseMoved((event) -> {
            DropShadow eff = new DropShadow(10, 0, 4, Color.valueOf("#359bed"));
            eff.setHeight(25);eff.setWidth(65); eff.setSpread(0.25);
            time.setEffect(eff);
        });
        time.setOnMouseExited((event) -> {
            time.setEffect(new Reflection(0,0.5, 0.5, 0.1));
        });
        
        bar.setOnMouseMoved((MouseEvent event) -> {
            setTooltip(Method.toTime((int)(event.getX()*dur/max)), event.getX()*dur/max, event.getSceneX(), event.getSceneY());
        });
        
        bar.setOnMouseExited((event) -> {
            tooltip.setVisible(false);
            if(preview!=null)preview.setVisible(false);
        });
        
        bar.setOnMousePressed((event) -> {
            double seekValue = event.getX()*dur/max;
            player.seek(seekValue);
            setValue(seekValue);
        });
        
        value.setOnMouseMoved((MouseEvent event) -> {
            setTooltip(Method.toTime((int)(event.getX()*dur/max)), event.getX()*dur/max, event.getSceneX(), event.getSceneY());
        });
        
        value.setOnMouseExited((event) -> {
            tooltip.setVisible(false);
            if(preview!=null)preview.setVisible(false);
        });
        
        value.setOnMousePressed((event) -> {
            double seekValue = event.getX()*dur/max;
            player.seek(seekValue);
            setValue(seekValue);
        });
    }
    
    /**
     * Cài đặt lại tổng thời gian của track cho progress
     * @param d giá trị của tổng thời gian mới
     */
    protected synchronized void setDuration(int d){
        // new value must be > 0 and different than old value
        if(d>0 && d != dur) dur = d;
    }
    
    /**
     * Cài đặt lại giá trị của tiến trình
     * @param now giá trị hiện tại của tiến trình chơi nhạc
     */
    protected synchronized void setValue(double now){
        if(now<0 || now>dur) now = 0;
        value.setWidth(now*max/dur);
        time.setText(Method.toTime((int)now) + " | " + Method.toTime((int)dur));
        time.setTranslateX(now*(max-80)/dur);
        if(!time.isVisible() && !isFullScreen)time.setVisible(true);
    }
    
    /**
     * Cài đặt lại media-view cho chế độ preview của track Video
     * @param mv media-view hiện tại của track
     */
    protected void setPreview(MediaView mv){
        preview = mv;
        if(mv!=null){
            preview.setFitHeight(80);preview.setFitWidth(80*16/9);
            preview.setPreserveRatio(true);
            preview.setId("preview-video");mv.setSmooth(true);
            preview.setEffect(new DropShadow());
            preview.setVisible(false);
            root.getChildren().add(preview);
        }
    }
    
    /**
     * Cài đặt hiển thị thời gian hiện tại mà người dùng đang trỏ tới
     * @param txt thời gian hiện tại của hệ thống (cho hiển thị)
     * @param currentTime thời gian hiện tại của hệ thống (cho previewVideo)
     * @param x tọa độ x của con trỏ chuột hiện tại
     * @param y tọa độ y của con trỏ chuột hiện tại
     */
    private void setTooltip(String txt, double currentTime, double x, double y){
        tooltip.setText(txt);
        if(x+30 > max) x = max-30;
        tooltip.setTranslateX(x - 30);tooltip.setTranslateY(y - 25 + (isFullScreen?0:-30));
        tooltip.setVisible(true);
        
        if(preview!=null){
            preview.getMediaPlayer().seek(Duration.seconds(currentTime));
            preview.setX(x - preview.getFitWidth()/2); preview.setY(y - 10 + (isFullScreen?0:-30) - preview.getFitHeight());
            preview.setVisible(true);
        }
        
        if(preview!=null)preview.toFront();
        tooltip.toFront();
    }
    
    /**
     * 
     * @return giá trị tọa độ y của progress
     */
    protected double getY() {
        return y;
    }
    
    /**
     * Cài đặt có cho progress hiển thị hay không
     * @param val có cho phép hiển thị không
     */
    protected void setVisible(boolean val){
        bar.setVisible(val);value.setVisible(val);
        time.setVisible(val);
    }
    
    /**
     * Cài đặt chế độ fullscreen
     * @param w chiều rộng màn hình
     * @param h chiều cao màn hình
     */
    protected void setFullScreen(double w, double h){
        bar.setWidth(w - 40);
        max = w - 40;
        
        //  position
        bar.setY(h - 30);
        value.setY(h - 30);
        time.setTranslateY(h - 50);
        
        time.setId("time-custom-fullscreen");
        
        //  index
        toFront();
        
        isFullScreen = true;
    }
    
    /**
     * Đặt progress lên trên hết (trong hiển thị trên giao diện)
     */
    protected void toFront(){
        time.toFront();
        bar.toFront();
        value.toFront();
        tooltip.toFront();
        if(preview!=null) preview.toFront();
    }
    
    /**
     * thoát khỏi chế độ fullscreen
     */
    protected void exitFullScreen(){
        bar.setWidth(defaultW);
        max = defaultW;
        
        bar.setY(y);
        value.setY(y);
        time.setTranslateY(y - 20);
        
        time.setId("time-custom");
        
        isFullScreen = false;
    }
    
    /**
     * Đặt progress xuống dưới đáy của giao diện
     */
    protected void toBack(){
        if(preview!=null) preview.toBack();
        tooltip.toBack();
        value.toBack();
        bar.toBack();
        time.toBack();
    }
    
    /**
     * Cài đặt màu hiển thị của progress khi người dung thay đổi cài đặt
     */
    protected void updateColor() {
        value.setFill(Method.User.getProgressColor());
        time.setTextFill(Method.User.getProgressColor());
        
        bar.setEffect(new DropShadow(3, Method.User.getProgressColor()));
        value.setEffect(new DropShadow(1, Method.User.getProgressColor()));
    }
}