/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nextplayer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Separator;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * các phương thức, công cụ cho chương trình
 *
 * @author nmhillusion
 */
final class Method {

    /**
     * Method will create a font
     *
     * @param name name of font, include Dosis, Jose
     * @param size size of font
     * @return Font after load from resource
     */
    protected static Font getFont(String name, int size) {
        Font f = null;
        switch (name) {
            case "time":
                f = Font.loadFont("file:resource/Dosis-Regular.ttf", (size < 1) ? 11 : size);
                break;

            case "general":
                f = Font.loadFont("file:resource/Quicksand-Medium.ttf", (size < 1) ? 12 : size);
                break;

            default:
                Method.dialog("alert", "Font is not available!");
                break;
        }

        return f;
    }

    /**
     * Method to convert seconds to time
     *
     * @param x : numbers of seconds
     * @return : String is the time after convert
     */
    protected static String toTime(int x) {
        int hh = x / 3600;
        x -= hh * 3600;
        int mm = x / 60;
        x -= mm * 60;
        int ss = x;

        return fill2char(hh) + ":" + fill2char(mm) + ":" + fill2char(ss);
    }

    /**
     * Lấp đầy 2 kí tự, trong định dạng thời gian chơi
     *
     * @param x số ban đầu
     * @return số sau khi điều chỉnh
     */
    private static String fill2char(int x) {
        if (x < 10) {
            return "0" + x;
        } else {
            return String.valueOf(x);
        }
    }

    /**
     * Method will create a random Color
     *
     * @return : a string HEX code of color
     */
    protected static String getRandomColor() {
        String res = "#";
        for (int i = 0; i < 6; ++i) {
            int x = Math.round(Math.round(Math.random() * 15));
            if (x > 9) {
                res += (char) ('A' + x - 10);
            } else {
                res += x;
            }
        }
        return res;
    }

    /**
     * Lấy ảnh về từ một đường dẫn file
     *
     * @param path đường dẫn của file ảnh
     * @return ảnh được lấy về
     */
    protected static Image ImageFromFile(String path) {
        Image img = null;
        try {
            if (path.compareTo("null") == 0) {
                return null;
            }
            img = new Image(new File(path).toURI().toURL().toString());
        } catch (java.net.MalformedURLException ex) {
            Method.dialog("alert", "Path to image is not available!");
        }
        return img;
    }

    /**
     * Hiện lên popup để lấy ảnh từ trên máy
     *
     * @return file sau khi người dùng chọn xong
     */
    protected static File chooseImage() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open File(s):");
        FileChooser.ExtensionFilter allSupport = new FileChooser.ExtensionFilter("All file image", "*.png", "*.jpg", "*.jpeg");
        fc.getExtensionFilters().addAll(allSupport);
        fc.setInitialDirectory(new File("\\"));
        return fc.showOpenDialog(null);
    }

    /**
     * Giới hạn độ dài tối đa của chuỗi
     *
     * @param str chuỗi ban đầu
     * @param end độ dài tối đa mà bạn muốn hiển thị
     * @return chuỗi sau khi đã giới hạn
     */
    protected static String limitString(String str, int end) {
        if (end > str.length()) {
            return str;
        }
        return str.substring(0, end) + "...";
    }

    /**
     * Method will create a dialog to user
     *
     * @param type : is type of dialog, includes: alert, confirm, prompt
     * @param txt : content of dialog
     */
    protected static Object dialog(String type, String txt) {
        Object res = null;
        if (type.compareTo("alert") == 0) {
            Alert dialog = new Alert(Alert.AlertType.WARNING, txt, ButtonType.CLOSE);
            dialog.showAndWait();
        } else if (type.compareTo("info") == 0) {
            Alert dialog = new Alert(Alert.AlertType.INFORMATION, txt, ButtonType.CLOSE);
            dialog.showAndWait();
        } else if (type.compareTo("confirm") == 0) {
            Alert dialog = new Alert(Alert.AlertType.CONFIRMATION, txt, ButtonType.YES, ButtonType.NO);
            Optional<ButtonType> result = dialog.showAndWait();
            if (result.get() == ButtonType.YES) {
                res = 0;
            } else {
                res = 1;
            }
        } else if (type.compareTo("prompt") == 0) {
            TextInputDialog dialog = new TextInputDialog();
            Optional<String> result = dialog.showAndWait();
            res = result.get();
        }

        return res;
    }

    /**
     * Cài đặt cơ chế look-and-feel
     */
    protected static void lookAndFill() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex) {
            Method.dialog("alert", "// error in look and feel");
        }
    }

    /**
     * Lớp người dùng phần mềm và các thiết lập phần mềm của họ
     */
    protected static class User {

        private static String defaultBackground, defaultCoverImg, progressColor, equalizerColor, backgroundColor;
        private static boolean previewVideo, isShakeEffect;
        private static int timeSkip;

        protected static void setUp(String _progressColor, String _equalizerColor, String _backgroundColor, boolean _previewVideo, boolean _isShakeEffect, int _timeSkip) {
            progressColor = _progressColor;
            equalizerColor = _equalizerColor;
            backgroundColor = _backgroundColor;
            previewVideo = _previewVideo;
            isShakeEffect = _isShakeEffect;
            timeSkip = _timeSkip;
        }

        protected static Image getDefaultBackground() {
            return ImageFromFile(defaultBackground);
        }

        protected static Image getDefaultCoverImg() {
            return ImageFromFile(defaultCoverImg);
        }

        protected static Color getProgressColor() {
            return Color.valueOf(progressColor);
        }

        protected static Color getEqualizerColor() {
            return Color.valueOf(equalizerColor);
        }

        protected static Color getBackgroundColor() {
            return Color.valueOf(backgroundColor);
        }

        protected static boolean isPreviewVideo() {
            return previewVideo;
        }

        protected static boolean isShakeEffect() {
            return isShakeEffect;
        }

        protected static int getTimeSkip() {
            return timeSkip;
        }
    }

    /**
     * Tạo ra menu setting khi người dùng click vào cài đặt
     *
     * @param btn button mà menu này phụ thuộc
     * @param x tọa dộ x của button đó
     * @param y tọa độ y của button đó
     * @param rootPlayer thể hiện của chương trình này
     */
    protected static void createContextMenuSetting(Button btn, short x, short y, NextPlayer rootPlayer) {
        MenuItem setting = new MenuItem("Setting Player"),
                about = new MenuItem("About Author");

        setting.setOnAction((event) -> {
            openSetting(rootPlayer);
        });
        setting.setAccelerator(new KeyCodeCombination(KeyCode.S));

        about.setOnAction((event) -> {
            Stage dialog = new Stage(StageStyle.TRANSPARENT);

            Label info = new Label("\t\t- next player - v1.0\n"
                    + "\tauthor: nmhillusion (Nguyen Minh Hieu)\n"
                    + "\temail: nguyenminhhieu.geek@gmail.com\n\n"
                    + "\tprogram is developed with java 8\n");
            info.setFont(getFont("general", 18));
            info.setStyle("-fx-text-fill: #eff");
            info.setEffect(new Reflection());
            info.setPrefSize(400, 250);
            info.setAlignment(Pos.CENTER);

            ImageView bg = new ImageView(ImageFromFile("resource/info-background.jpg"));
            bg.setFitWidth(370);
            bg.setFitHeight(210);
            bg.setStyle("-fx-border-radius: 8;-fx-background-radius: 8;-fx-border-color: red");
            bg.setEffect(new DropShadow());

            Button close = new Button("Close");
            close.setPrefSize(50, 20);
            close.setTranslateX(150);
            close.setTranslateY(100);
            close.setOnAction((_event) -> {
                dialog.close();
            });

            StackPane pan = new StackPane(info, close);
            pan.setStyle("-fx-fill: rgba(0,0,0,0);-fx-background-color: rgba(0,0,0,0)");
            pan.getChildren().add(0, bg);
            Scene scene = new Scene(pan);
            scene.setFill(Color.valueOf("rgba(0,0,0,0.2)"));
            dialog.setScene(scene);
            dialog.show();
        });
        about.setAccelerator(new KeyCodeCombination(KeyCode.A));

        ContextMenu cm = new ContextMenu(setting, about);
        cm.setId("option-player");
        cm.show(btn, x, y);
    }

    static boolean resetSetting = false;

    /**
     * Mở ra menu cai đặt cho phần mềm
     *
     * @param rootPlayer thể hiện của phần mềm này
     */
    private static void openSetting(NextPlayer rootPlayer) {
        Stage dialog = new Stage(StageStyle.UTILITY);
        dialog.setTitle("Settings");
        ScrollPane rootD = new ScrollPane();
        rootD.setId("dialog-setting");
        Scene sceneD = new Scene(rootD);
        sceneD.getStylesheets().add("nextPlayer/stylePlayer.css");
        sceneD.setFill(Paint.valueOf("#333"));
        GridPane gridPane = new GridPane();
        gridPane.setPadding(new Insets(20));
        gridPane.setHgap(8);
        gridPane.setVgap(8);
        {
            Label infoTrack = new Label("Background Color: "),
                    progress = new Label("Process Color: "),
                    equalizer = new Label("Equalizer Color:");

            infoTrack.setFont(getFont("general", 0));
            progress.setFont(getFont("general", 0));
            equalizer.setFont(getFont("general", 0));

            ColorPicker getBackgroundColor = new ColorPicker(Color.valueOf(User.backgroundColor)),
                    getColorEqualizer = new ColorPicker(Color.valueOf(User.equalizerColor)),
                    getColorProgress = new ColorPicker(Color.valueOf(User.progressColor));

            CheckBox cbPreVid = new CheckBox("use preview video"), cbShake = new CheckBox("use shake effect");
            cbPreVid.setSelected(User.previewVideo);
            cbShake.setSelected(User.isShakeEffect);

            gridPane.addRow(0, infoTrack, getBackgroundColor);
            gridPane.addRow(1, progress, getColorProgress);
            gridPane.addRow(2, equalizer, getColorEqualizer);
            gridPane.add(new Separator(), 0, 3, 2, 1);
            gridPane.addRow(4, cbPreVid, cbShake);
            gridPane.add(new Separator(), 0, 5, 2, 1);

            Spinner<Integer> spinTime = new Spinner<>(1, 30, User.timeSkip, 1);
            gridPane.addRow(6, new Label("Time Skip"), spinTime);
            gridPane.add(new Separator(), 0, 7, 2, 1);

            TextField txtBg = new TextField(User.defaultBackground),
                    txtCover = new TextField(User.defaultCoverImg);
            txtBg.setPrefSize(250, 30);
            txtCover.setPrefSize(250, 30);
            txtBg.setDisable(true);
            txtCover.setDisable(true);
            Button btnBg = new Button("Choose Background Default"),
                    btnCover = new Button("Choose Cover Default");
            btnBg.setOnAction((event) -> {
                String txt;
                File obj = chooseImage();
                if (obj != null) {
                    txt = User.defaultBackground = obj.getAbsolutePath();
                    txtBg.setText(txt);
                }
            });
            btnCover.setOnAction((event) -> {
                String txt;
                File obj = chooseImage();
                if (obj != null) {
                    txt = User.defaultCoverImg = obj.getAbsolutePath();
                    txtCover.setText(txt);
                }
            });
            btnBg.setMinWidth(150);
            btnCover.setMinWidth(150);
            gridPane.addRow(8, txtBg, btnBg);
            gridPane.addRow(9, txtCover, btnCover);
            GridPane.setFillWidth(btnBg, true);
            GridPane.setFillWidth(btnCover, true);

            resetSetting = false;
            Button btnReset = new Button("Reset Default");
            btnReset.setId("btnReset");
            btnReset.setOnAction((event) -> {
                try {
                    Properties properties = new Properties();
                    properties.load(new FileInputStream("default_setting.nxt"));
                    String dfBackground = properties.getProperty("defaultBackground", "resource/background.png"),
                            dfCover = properties.getProperty("defaultCoverImg", "resource/nmhlogo.png"),
                            progressColor = properties.getProperty("progressColor", "#359bed"),
                            equalizerColor = properties.getProperty("equalizerColor", "#359bed"),
                            backgroundColor = properties.getProperty("backgroundColor", "#333333");
                    boolean isPreviewVideo = Boolean.valueOf(properties.getProperty("previewVideo", "true")),
                            isShakeEffect = Boolean.valueOf(properties.getProperty("shakeEffect", "true"));
                                        
                    int timeskip = Integer.parseInt(properties.getProperty("timeSkip", "5"));
                    getBackgroundColor.setValue(Color.valueOf(backgroundColor));
                    getColorEqualizer.setValue(Color.valueOf(equalizerColor));
                    getColorProgress.setValue(Color.valueOf(progressColor));
                    cbPreVid.setSelected(isPreviewVideo);
                    cbShake.setSelected(isShakeEffect);
                    spinTime.getValueFactory().setValue(timeskip);
                    txtBg.setText(dfBackground);
                    txtCover.setText(dfCover);
                    resetSetting = true;
                } catch (IOException ex) {
                    dialog("alert", "Can not get data from file setting! Error: " + ex.getLocalizedMessage());
                }
            });

            Button btnSave = new Button("Save");
            btnSave.setId("btnSave");
            btnSave.setOnAction((event) -> {
                User.setUp(getColorProgress.getValue().toString(), getColorEqualizer.getValue().toString(), getBackgroundColor.getValue().toString(), cbPreVid.isSelected(), cbShake.isSelected(), spinTime.getValue());
                if (resetSetting) {
                    User.defaultBackground = txtBg.getText();
                    User.defaultCoverImg = txtCover.getText();
                }
                dialog.close();
                //  update to GUI
                rootPlayer.updateSetting();

                //  save to file
                updateSetting();
            });
            HBox hboxBtn = new HBox(btnReset, btnSave);
            hboxBtn.setSpacing(20);
            hboxBtn.setPrefWidth(dialog.getWidth());
            hboxBtn.setAlignment(Pos.CENTER);

            gridPane.add(new Separator(), 0, 10, 2, 1);
            gridPane.add(hboxBtn, 0, 11, 2, 1);
        }

        gridPane.heightProperty().addListener((observable) -> {
            dialog.setWidth(gridPane.getWidth() + 40);
            dialog.setHeight(gridPane.getHeight() + 40);
            rootD.setPrefSize(gridPane.getWidth() + 40, gridPane.getHeight() + 40);
        });

        rootD.setContent(new StackPane(gridPane));
        dialog.setScene(sceneD);
        dialog.show();
    }

    /**
     * This function to load user's setting from file If one in them is null
     * will set default by program
     */
    protected static void loadSetting() {
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream("user_setting.nxt"));
            String dfBackground = properties.getProperty("defaultBackground", "resource/background.png"),
                    dfCover = properties.getProperty("defaultCoverImg", "resource/nmhlogo.png"),
                    progressColor = properties.getProperty("progressColor", "#359bed"),
                    equalizerColor = properties.getProperty("equalizerColor", "#359bed"),
                    backgroundColor = properties.getProperty("backgroundColor", "#333333");
            boolean isPreviewVideo = Boolean.valueOf(properties.getProperty("previewVideo", "true")),
                    isShakeEffect = Boolean.valueOf(properties.getProperty("shakeEffect", "true"));
            int timeskip = Integer.parseInt(properties.getProperty("timeSkip", "5"));
            
            User.defaultBackground = dfBackground;
            User.defaultCoverImg = dfCover;
            User.progressColor = progressColor;
            User.equalizerColor = equalizerColor;
            User.backgroundColor = backgroundColor;
            User.previewVideo = isPreviewVideo;
            User.isShakeEffect = isShakeEffect;
            User.timeSkip = timeskip;

        } catch (IOException e) {
            dialog("alert", "Can not read setting from file!");
            
            System.out.println("..loading setting when having exception.. ");
            User.defaultBackground = "resource/background.png";
            User.defaultCoverImg = "resource/nmhlogo.png";
            User.progressColor = "#359bed";
            User.equalizerColor = "#359bed";
            User.backgroundColor = "#ff9999";
            User.previewVideo = true;
            User.isShakeEffect = true;
            User.timeSkip = 8;
        }
    }

    /**
     * Cập nhật cài đặt người dùng vào cơ sở dữ liệu
     */
    private static void updateSetting() {
        try {
            Properties properties = new Properties();
            properties.put("defaultBackground", User.defaultBackground);
            properties.put("defaultCoverImg", User.defaultCoverImg);
            properties.put("progressColor", User.progressColor);
            properties.put("equalizerColor", User.equalizerColor);
            properties.put("backgroundColor", User.backgroundColor);
            properties.put("previewVideo", String.valueOf(User.previewVideo));
            properties.put("shakeEffect", String.valueOf(User.isShakeEffect));
            properties.put("timeSkip", String.valueOf(User.timeSkip));
            properties.store(new FileOutputStream("user_setting.nxt"), "This are user's settings");
        } catch (IOException e) {
            dialog("alert", "Can not read setting from file! Error: " + e.getMessage());
        }
    }
}
