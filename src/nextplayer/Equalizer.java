package nextplayer;

import java.util.LinkedList;
import java.util.List;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Reflection;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

/**
 *  Bộ điều chỉnh âm lượng
 * @author nmhillusion
 */
final class VolumeSlider extends Group{
    private static Rectangle bar, value;
    private static int max;
    /**
     * Khởi tạo cho bộ điều chỉnh âm lượng
     * @param w chiều rộng ứng dụng
     * @param h chiều cao ứng dụng
     * @param player thể hiện của chương trình này
     */
    public VolumeSlider(int w, int h, NextPlayer player){
        super();
        bar = new Rectangle(w, h, Paint.valueOf("#222"));
        value = new Rectangle(w*0.8, h, Paint.valueOf("#eff"));
        getChildren().addAll(bar, value);
        
        max = w;
        
        bar.setEffect(new DropShadow(3, Color.AQUA));
        value.setEffect(new DropShadow(5, Color.valueOf("#36f")));
        bar.setArcHeight(h);bar.setArcWidth(h);
        value.setArcHeight(h);value.setArcWidth(h);
        bar.setCursor(Cursor.HAND);value.setCursor(Cursor.HAND);
        
        bar.setOnMousePressed((event) -> {
            double val = event.getX()*100/max;
            if(player.setVolume(val)){
                setValue(val,  player.isPlayer());
            }
        });
        
        value.setOnMousePressed((event) -> {
            double val = event.getX()*100/max;
            if(player.setVolume(val)){
                setValue(val, player.isPlayer());
            }
        });
    }
    
    /**
     * Cài đặt âm lượng
     * @param val giá trị âm lượng mới
     * @param canSet có thể cài đặt không
     */
    protected void setValue(double val, boolean canSet){
        if(canSet){
            val= val>0?(val<=100?val:100):0;
            value.setWidth(val*max/100);
        }
    }
}

/**
 * Bộ hiệu ứng nhịp điệu (equalizer)
 * @author nmhillusion
 */
final class Equalizer{
    private List<Rectangle> bars = new LinkedList<>();
    private static Group root;
    private int w, h;
    
    /**
     * Khởi tạo cho Equalizer
     * @param w chiều rộng của ứng dụng
     * @param h chiều cao của ứng dụng
     * @param _root ổ chứa chính của cả chương trình
     */
    protected Equalizer(int w, int h, Group _root){
        this.w = w; this.h = h;
        for(int i= 0;i< 128;++i){
            Rectangle r = new Rectangle(i*(w/150 + 2), 0, w/150, 0);
            r.setArcWidth(w/100);
            r.setArcHeight(w/100);
            r.setFill(Method.User.getEqualizerColor());
            r.setRotate(180);
            r.setEffect(new Reflection());
            bars.add(r);
        }
        
        root = _root;
        root.getChildren().addAll(bars);
    }
    
    /**
     * Đặt lại giá trị cho dải equalizer
     * @param values mảng các giá thô
     * @param minVal giá trị thấp nhất có thể có
     */
    protected void setValue(float[] values, float minVal){
        int i = 0;
        for(Rectangle bar : bars){
            bar.setHeight((values[i++] - minVal)*2);
        }
    }
    
    /**
     * Cài đặt sự hiển thị của equalizer
     * @param val có cho hiển thị không
     */
    protected void setVisible(boolean val){
        bars.forEach((b) -> {
            b.setVisible(val);
        });
    }
    
    /**
     * Cài đặt full màn hình
     * @param _w chiều rộng màn hình
     * @param _h chiều cao màn hình
     */
    protected void setFullscreen(double _w, double _h){
        int leng = bars.size(); double wb = _w/150;
        Rectangle b;
        for(int i=0;i<leng;++i){
            b = bars.get(i);
            b.setX(i*(wb + 2));
            b.setWidth(wb);
            b.setArcWidth(_w/100);
            b.setArcHeight(_w/100);
            b.toFront();
        }
    }
    
    /**
     * Thoát khỏi chế độ fullScreen
     */
    protected void exitFullscreen(){
        int leng = bars.size(); double wb = w/150;
        Rectangle b;
        for(int i=0;i<leng;++i){
            b = bars.get(i);
            b.setX(i*(wb + 2));
            b.setWidth(wb);
            b.setArcWidth(w/100);
            b.setArcHeight(w/100);
            b.toBack();
        }
    }
    
    /**
     * Cập nhật lại màu khi người dưng thay đổi cài đặt
     */
    protected void updateBarColor() {
        bars.forEach((b) -> {
            b.setFill(Method.User.getEqualizerColor());
        });
    }
}
