package nextplayer;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;

/**
 * Danh sách nhạc mà người dùng đang chơi
 *
 * @author nmhillusion
 */
final public class PlaylistNowPlaying extends Playlist<Track> {

    private int sum;
    private boolean loop, shuffle, isReady;
    private static Label displayInfo = new Label();
    private static NextPlayer rootPlayer;
    private static ScrollPane listPane = new ScrollPane();
    private List<Track> listTrack;
    private List<Integer> listPrevious, listPlayed;
    private Track idx = null;
    private static VBox vbox;

    /**
     * Khởi tạo cho playlist
     *
     * @param l danh sách các file của playlist
     * @param srcImg ảnh nền của playlist
     * @param shuf có chơi các track trong playlist một cách ngẫu nhiên không
     * @param width chiều rộng của ứng dụng
     * @param height chiều cao của ứng dụng
     * @param root ổ chứa chính của chương trình
     */
    public PlaylistNowPlaying(List<File> l, Image srcImg, boolean shuf, int width, int height, Group root) {
        super("Now Playing");
        listPrevious = new LinkedList<>();
        listPlayed = new LinkedList<>();
        int length = l.size();

        shuffle = shuf;

        if (srcImg != null) {
            rootPlayer.setBackground(srcImg);
        }

        displayInfo.setPrefSize(width / 2 - 10, height / 5);
        displayInfo.setTranslateX(width / 2 + 5);
        if (!root.getChildren().contains(displayInfo)) {
            root.getChildren().add(displayInfo);
        }

        sum = 0;
        isReady = false;
        vbox.getChildren().clear();
        Track.init(vbox, root, rootPlayer);
        listTrack = getListTrack();
        //  TODO: check get List track
        l.forEach((file) -> {
            createTrack(file, width, height, length);
        });

        listPane.setVmin(0);
        vbox.heightProperty().addListener((observable, oldValue, newValue) -> {
            listPane.setVmax(vbox.getLayoutBounds().getHeight());
        });
        vbox.setPrefWidth(width);
        listPane.setContent(vbox);
    }

    /**
     * Khởi tạo lại cho playlist
     *
     * @param tab tab sẽ chứa các track trong playlist
     * @param rPlayer thể hiện của chương trình này
     */
    protected static void init(Tab tab, NextPlayer rPlayer) {
        listPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        listPane.setId("listPane");
        tab.setContent(listPane);

        vbox = new VBox();
        vbox.setFillWidth(true);
        vbox.setCursor(Cursor.HAND);
        vbox.setSpacing(2);
        
        vbox.setOnDragOver((DragEvent event) -> {
            System.out.println("it here drag on list songs: " + event.getGestureSource());
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            if (event.getGestureSource() != vbox
                    && event.getDragboard().hasString()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }
            event.consume();
        });

        vbox.setOnDragDropped((DragEvent event) -> {
            Dragboard db = event.getDragboard();
            boolean success = false;
            if (db.hasString()) {
                System.out.println(" drop event player: " + db.getString());
                success = true;
            }
            System.out.println(event + " drop event player 2: " + db);
            /* let the source know whether the string was successfully
            * transferred and used */
            event.setDropCompleted(success);
            
            event.consume();
        });

        rootPlayer = rPlayer;

        displayInfo.setFont(Method.getFont("general", 0));
        displayInfo.setId("infoTrack");
        displayInfo.setAlignment(Pos.CENTER);
        displayInfo.setTranslateY(10);
        Tooltip tooltip = new Tooltip();
        tooltip.setStyle("-fx-text-fill: #36f;");
        tooltip.setFont(Method.getFont("general", 0));
        displayInfo.setTooltip(tooltip);
    }

    /**
     * Tạo ra một track của playlist
     *
     * @param f file của track sẽ tạo
     * @param width chiều rộng của ứng dụng
     * @param height chiều cao của ứng dụng
     * @param length chiều dài của ứng dụng
     */
    private void createTrack(File f, int width, int height, int length) {
        try {
            Media md = new Media(f.toURI().toString());
            md.setOnError(() -> {
                Method.dialog("alert", "Media: Error when loading file!");
            });

            /*
                    16 - 9
                    sw - sh - correct

                    ow - oh  - origin
                    nw - sh	 - edit

                    => nw = sh * ow / oh

                    => offy = (sw - nw)/2
             */
            Track track = new Track(md, width, height, this);

            listTrack.add(track);
            System.out.println(">>> loaded " + (sum + 1) + "/" + length);
            if ((++sum) == length) {
                Platform.runLater(() -> {
                    isReady = true;
                    rootPlayer.startPlaying();
                });
            }
        } catch (Exception ex) {
            Method.dialog("alert", "[playlistnowplaying] Error in playlist: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     *
     * @return track đang được chỉ định chạy của playlist
     */
    protected Track nowPlay() {
        try {
            if (idx == null) {
                idx = next();
            }

            displayInfo.setText(idx.setInfo());
            displayInfo.getTooltip().setText("\t- info track -"
                + "\n[title]: " + idx.getTitle()
                + "\n[artist]: " + idx.getArtist()
                + "\n[composer]: " + idx.getComposer()
                + "\n[album]: " + idx.getAlbum()
                + "\n[year]: " + idx.getYear());
            idx.playing();
            if (!listPlayed.contains(idx.hashCode())) {
                listPlayed.add(idx.hashCode());
            }

            //  25 is height of row
            listPane.setVvalue(listTrack.indexOf(idx) * 28);
        } catch (ArrayIndexOutOfBoundsException ex) {
            Method.dialog("alert", "Not found nowPlay!");
        }
        return idx;
    }

    /**
     * @return track tiếp theo của playlist
     */
    protected Track next() {
        if (idx != null) {
            addPreviousTrack();
        }

        if (listPlayed.size() > 0 && listPlayed.size() == listTrack.size()) {
            if (loop) {
                listPlayed.clear();
            } else {
                return null;
            }
        }

        if (!shuffle) {
            if (isEnd()) {    // last element?
                if (loop) {
                    idx = listTrack.get(0);
                } else {
                    idx = null;
                }
            } else if (listTrack.size() > listTrack.indexOf(idx) + 1) {
                idx = listTrack.get(listTrack.indexOf(idx) + 1);
            } else {
                return null;
            }
        } else {
            do {
                idx = listTrack.get(Math.round(Math.round(Math.random() * (listTrack.size() - 1))));
            } while (listPlayed.contains(idx.hashCode()));
        }
        return idx;
    }

    /**
     *
     * @return track trước đó của playlist
     */
    protected Track previous() {
        if (!listPrevious.isEmpty()) {
            if (idx != null) {
                idx.setDefault();
            }
            idx = getTrackFromHashCode(((LinkedList<Integer>) listPrevious).removeLast());

            return idx;
        } else {
            return idx = null;
        }
    }

    @Override
    protected void seekTrack(Track seek) {
        addPreviousTrack();
        if (listTrack.contains(seek)) {
            idx = seek;
            System.out.println(">> seek track: " + seek.getTitle());
        }
        rootPlayer.play();
    }

    /**
     * Thực hiện nhảy đến track cụ thể qua chỉ số của nó
     *
     * @param seek chỉ số của track sẽ nhảy đển
     */
    protected void seekTrack(short seek) {
        addPreviousTrack();
        if (0 <= seek && seek < listTrack.size()) {
            idx = listTrack.get((int) seek);
        }
        rootPlayer.play();
    }

    /**
     * Thêm vào danh sách các track trước đó
     */
    private void addPreviousTrack() {
        if (listPrevious.size() > 50) {
            listPrevious.remove(0);
        }

        if (idx != null) {
            idx.setDefault();
            listPrevious.add(idx.hashCode());
        }
    }

    /**
     * Lấy về một track thông qua hashcode
     *
     * @param hashcode hashcode của track sẽ được lấy về
     * @return track được lấy về
     */
    private Track getTrackFromHashCode(int hashcode) {
        Track tr = null;
        for (Track t : listTrack) {
            if (t.hashCode() == hashcode) {
                return t;
            }
        }
        return tr;
    }

    @Override
    protected void unSelectTracks() {
        listTrack.forEach((t) -> {
            if (t.isSelected()) {
                t.unSelect();
            }
        });
    }

    /**
     * get tracks was selected by source
     *
     * @return list source of track seleced
     */
    protected List<String> getSourceSelected() {
        List<String> res = new LinkedList<>();

        listTrack.forEach((t) -> {
            if (t.isSelected()) {
                res.add(t.getSource());
            }
        });

        return res;
    }

    /**
     * get tracks was selected by index
     *
     * @return list index of track seleced
     */
    protected List<Track> getTrackSelected() {
        List<Track> res = new LinkedList<>();
        listTrack.forEach((t) -> {
            if (t.isSelected()) {
                res.add(t);
            }
        });
        return res;
    }

    /**
     * Cài đặt trạng thái chơi ngẫu nhiên của playlist
     *
     * @param val có chơi ngẫu nhiên không
     */
    protected void setShuffle(boolean val) {
        shuffle = val;
    }

    /**
     * Cài đặt vòng lặp
     *
     * @param val có lặp lại cả playlist này không
     */
    protected void setLoop(boolean val) {
        loop = val;
    }

    @Override
    protected boolean deleteDisplay(Track t, int pos) {
        if (t.delete()) {
            short oldVvalue = (short) listPane.getVvalue();
            if (listPrevious.contains(t.hashCode())) {
                listPrevious.remove((Integer) t.hashCode());
                listPlayed.remove((Integer) t.hashCode());
            }

            vbox.heightProperty().addListener((observable) -> {
                listPane.setVvalue(oldVvalue);
            });
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected boolean canDelete(Track t) {
        return t.canDelete();
    }

    @Override
    protected void numbericList() {
        for (int i = 0, len = listTrack.size(); i < len; ++i) {
            listTrack.get(i).setNumber(i + 1);
        }
    }

    /**
     * @return Playlist đã sẵn sàng hay chưa
     */
    protected boolean isReady() {
        return isReady;
    }

    /**
     * @return có chơi ngẫu nhiên không
     */
    protected boolean isShuffle() {
        return shuffle;
    }

    /**
     * @return có kết thúc playlist này chưa
     */
    protected boolean isEnd() {
        return !listTrack.isEmpty() && (listTrack.indexOf(idx) == (listTrack.size() - 1));
    }
}
