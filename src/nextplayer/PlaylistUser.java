package nextplayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author nmhillusion
 */
final public class PlaylistUser extends Playlist<String>{
    private List<String> listTrack;
    private static ScrollPane listPane = new ScrollPane();
    private static VBox vbox;
    private static NextPlayer rootPlayer;
    private ImageView background = new ImageView();
    private ItemRow rowList;
    private short idx, width, oldVvalueTrack;
    private boolean isEdited = false;
    
    /**
     * Khởi tạo cho Playlist User
     * @param name tên của playlist
     * @param w chiều rộng của ứng dụng
     * @param tab tab sẽ chứa danh sách các playlist người dùng
     * @param rootPlayer thể hiện chính của chương trình đang thực thi
     */
    protected PlaylistUser(String name, int w, Tab tab, NextPlayer rootPlayer){
        super(name);
        tab.setContent(listPane);
        init((vbox==null)?1:(short)(vbox.getChildren().size()+1), w, rootPlayer);
    }
    
    /**
     * Khởi tạo cho Playlist User
     * @param name tên của playlist
     * @param bg đường dẫn ảnh nền của playlist
     * @param w chiều rộng của ứng dụng
     * @param tab tab sẽ chứa danh sách các playlist người dùng
     * @param rootPlayer thể hiện chính của chương trình đang thực thi
     */
    protected PlaylistUser(String name, String bg, int w, Tab tab, NextPlayer rootPlayer){
        super(name, bg);
        tab.setContent(listPane);
        init((vbox==null)?1:(short)(vbox.getChildren().size()+1), w, rootPlayer);
    }
    
    /**
     * khởi tạo lại class PlaylistUser
     */
    protected static void reset(){
        vbox = new VBox();
    }
    
    /**
     * Các khởi tạo cho ừng playlist
     * @param num số thứ tự playlist
     * @param w chiều rộng ứng dụng
     * @param _rootPlayer thể hiện chính của trình chơi nhạc hiện tại
     */
    private void init(short num, int w, NextPlayer _rootPlayer){
        rootPlayer = _rootPlayer;
        width = (short)w;
        idx = num;
        listTrack = getListTrack();
        
        vbox.setPrefWidth(w);
        vbox.setFillWidth(true);
        vbox.setSpacing(2);
        listPane.setContent(vbox);
        listPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        listPane.setId("listPane-Playlist");
    }
    
    @Override
    protected boolean canDelete(String t){ return true;}
    @Override
    protected boolean deleteDisplay(String t, int pos){
        if(t==null){
            isEdited = true; short oldV = (short)listPane.getVvalue();
            vbox.getChildren().remove(rowList);
            vbox.heightProperty().addListener((observable) -> {
                listPane.setVvalue(pos * 29);listPane.setVvalue(oldV);
            });
        }
        return true;
    }
    
    /**
     * Đưa thể hiện của playlist này như một item trong tab
     */
    protected void addToTabPane(){
        rowList = new ItemRow("[" + idx + "]. ", Method.limitString(getName(),45), listTrack.size() + " tracks");
        rowList.setup(width - 45, 25, this, createContextMenuPlaylist(), null);
        vbox.getChildren().add(rowList);
    }
    
    /**
     * Thêm một track mới váo playlist
     * @param source đường dẫn của track mới
     * @return có thêm được hay không
     */
    protected boolean add(String source){
        if(source.trim().compareTo("")!=0){
            listTrack.add(source);
            isEdited = true;
        }else{
            Method.dialog("alert", "Sorry, Can not add!");
            return false;
        }
        savePlaylist();
        return true;
    }
    
    /**
     * Thêm đông thời nhiều track vào playlist
     * @param listAdd danh sách của các track sẽ được thêm
     * @return có thêm được hay không
     */
    protected boolean addAll(List<String> listAdd){
        int leng = listAdd.size();
        for(int i=0; i < leng; ++i){
            if(listAdd.get(i).trim().compareTo("")!=0){
                listTrack.add(listAdd.get(i));
                isEdited = true;
            }else{
                Method.dialog("alert", "Sorry, Can not add these tracks!");
                return false;
            }
        }
        savePlaylist();
        return true;
    }
    
    /**
     * Lấy về một track cụ thể trong playlist
     * @param idx chỉ số của track muốn lấy
     * @return đường dẫn của track muốn lấy
     */
    protected String get(short idx){
        if(-1 < idx && idx < listTrack.size()){
            return listTrack.get(idx);
        }else return "";
    }
    
    /**
     * Tạo context menu khi nhấp phải chuột lên một playlist
     * @return context menu đã tạo
     */
    private ContextMenu createContextMenuPlaylist(){
        MenuItem edit = new MenuItem("edit"),
                delete = new MenuItem("delete");
        
        edit.setOnAction((event) -> {
            editTracks();
        });
        
        delete.setOnAction((event) -> {
            rootPlayer.deletePlaylist(this);
        });
        
        ContextMenu cm = new ContextMenu(edit,delete);
        cm.setId("edit-playlist-context");
        return cm;
    }
    
    /**
     * Bật lên một popup cho phép chỉnh sửa các track trong playlist
     */
    private void editTracks(){
        Stage dialog = new Stage(StageStyle.UTILITY);
        ScrollPane pan = new ScrollPane();
        Group root = new Group();
        pan.setId("edit-playlist");
        Scene scene = new Scene(pan);
        scene.getStylesheets().add("nextPlayer/stylePlayer.css");
        
        VBox vboxEdit = new VBox();
        vboxEdit.setSpacing(2);
        int leng = listTrack.size();
        
        if(Method.ImageFromFile(getBackground()) != null){
            background.setImage(Method.ImageFromFile(getBackground()));
        }else{
            scene.setFill(Paint.valueOf("#333"));
        }
        root.getChildren().add(background);
        
        Button btnBackground = new Button("Choose BackGround");
        btnBackground.setOnAction((event) -> {
            File srcImg = Method.chooseImage();
            if(srcImg!=null){
                setBackground(srcImg.getAbsolutePath());
                if(Method.ImageFromFile(getBackground())!=null){
                    if(background.getImage()!=null)background.setImage(Method.ImageFromFile(getBackground()));
                }else{
                    background.setImage(Method.ImageFromFile(getBackground()));
                }
            }
        });
        btnBackground.setAlignment(Pos.CENTER);
        vboxEdit.getChildren().add(btnBackground);
        
        listTrack.forEach((t) -> {
            Media media = new Media(t);
            MediaPlayer player = new MediaPlayer(media);
            player.setOnReady(() -> {
                String title = ""; Object o = media.getMetadata().get("title");
                if(o!=null && o.toString().trim().compareTo("")!=0 ){
                    title = media.getMetadata().get("title").toString();
                }else{
                    try {
                        title = java.net.URLDecoder.decode(media.getSource(), "UTF-8");
                        title = title.substring(title.lastIndexOf("/") + 1, title.lastIndexOf("."));
                    } catch (UnsupportedEncodingException ex) {
                        Method.dialog("alert", "Can not decode file name: " + media.getSource());
                    }
                }
                Label item = new Label(title);
                
                item.setContextMenu(createContextMenuTrack(t, vboxEdit, pan, item, title));
                
                item.setPrefWidth(480);
                vboxEdit.getChildren().add(item);
                
                deleteTemp(player,media);
            });
        });
        
        dialog.setTitle("// edit playlist: " + getName());
        root.getChildren().add(vboxEdit);
        pan.setContent(root);
        
        vboxEdit.heightProperty().addListener((observable) -> {
            pan.setVvalue(oldVvalueTrack);
        });
        
        pan.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scene.setFill(Color.TRANSPARENT);
        dialog.setScene(scene);
        dialog.setHeight(600);
        dialog.setWidth(500);
        
        if(background.getImage() != null){
            background.setFitWidth(dialog.getWidth());
            background.setFitHeight(600);
        }
        
        dialog.centerOnScreen();
        dialog.show();
    }
    
    /**
     * Tạo một context menu cho từng track trong menu chỉnh sửa playlist
     * @param t track đang được điều chỉnh/đường dẫn
     * @param vbox hộp chứa dọc
     * @param pan scrollpane
     * @param item label của track đang sửa
     * @param title - tiêu đề của file đang được chỉnh sửa
     * @return context menu đã tạo
     */
    private ContextMenu createContextMenuTrack(String t, VBox vbox, ScrollPane pan, Label item, String title){
        MenuItem label = new MenuItem("edit track: " + Method.limitString(title, 30)),
                switchUp = new MenuItem("switch up"),
                switchDown = new MenuItem("switch down"),
                delete = new MenuItem("delete");
        
        label.setStyle("-fx-border-radius: 3px;-fx-text-fill: #eff;-fx-border-color: #36f;-fx-border-width: 1;");
        
        switchUp.setOnAction((event) -> {
            switchUp(true, t, vbox, item);
        });
        switchDown.setOnAction((event) -> {
            switchUp(false, t, vbox, item);
        });
        
        delete.setOnAction((event) -> {
            deleteTrack(t);
            oldVvalueTrack = (short)pan.getVvalue();
            vbox.getChildren().remove(item);
        });
        
        ContextMenu cm = new ContextMenu(label,switchUp,switchDown,delete);
        cm.setId("edit-playlist-track-context");
        return cm;
    }
    
    /**
     * Chuyển vị trí của track trong danh sách
     * @param up có phải là chuyển lên không
     * @param t đường dẫn của track đang cần chuyển - chính là track đó trong danh sách
     * @param vbox
     * @param item label của track đang chuyển
     */
    private void switchUp(boolean up, String t, VBox vbox, Label item){
        int idxT = listTrack.indexOf(t), idxL = vbox.getChildren().indexOf(item);
        String o; Label oL;
        if(up && idxT > 0){
            o = listTrack.get(idxT - 1);
            oL = (Label)vbox.getChildren().get(idxL - 1);
        }else if(!up && idxT < listTrack.size()-1){
            o = listTrack.get(idxT + 1);
            oL = (Label)vbox.getChildren().get(idxL + 1);
        }else{
            Method.dialog("alert", "Can not move " + (up?"up":"down") + " from here!");
            return;
        }
        
        listTrack.set(idxT, o);listTrack.set(up?idxT-1:idxT+1, t);
        vbox.getChildren().set(idxL, new Label()); //  because vbox does not include duplicate children
        vbox.getChildren().set(up?idxL-1:idxL+1, item);
        vbox.getChildren().set(idxL, oL);
        System.gc();
    }
    
    /**
     * Lưu playlist này thành tập tin
     */
    protected void savePlaylist(){
        if(isEdited){
            try {
                try (PrintWriter out = new PrintWriter("playlist/" + getName() +".m3u", "utf-8")) {
                    out.println(getBackground());
                    listTrack.forEach((t) -> {
                        out.println(t);
                    });
                }
            } catch (FileNotFoundException|UnsupportedEncodingException ex) {
                Method.dialog("alert",">> error in write playlist");
            }
        }
    }

    @Override
    protected void unSelectTracks() {
        vbox.getChildren().forEach((t) -> {
            ((ItemRow)t).unSelect();
        });
    }
    
    @Override
    protected void seekTrack(String t){
        rootPlayer.playPlaylist(listTrack, getBackground());
    }
    
    /**
     * Đặt lại số thứ tự của playlist trên danh sách
     * @param value thứ tự mới của playlist
     */
    protected void setNumber(int value){
        rowList.setNumber("[" + value + "].");
    }
    
    @Override
    protected void numbericList(){
        List<PlaylistUser> list = rootPlayer.getPlaylists();
        for(int i=0, len = list.size(); i< len;++i){
            list.get(i).setNumber(i+1);
        }
    }
}
