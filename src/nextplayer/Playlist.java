package nextplayer;

import java.util.LinkedList;
import java.util.List;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * Đây là lớp playlist mẫu chung
 * @author nmhillusion
 */
abstract class Playlist<T>{
    private String srcImg = null, name = null;
    private List<T> listTrack = new LinkedList<>();
    
    /**
     * Phương thức khởi tạo playlist 2 tham số
     * @param name tên của playlist
     * @param srcImg đường dẫn của file ảnh nền của playlist
     */
    protected Playlist(String name, String srcImg){
        this.name = name; this.srcImg = srcImg;
    }
    
    /**
     * Phương thức khởi tạo playlist 1 tham số
     * @param name tên của playlist
     */
    protected Playlist(String name){
        this.name = name;        
    }
    
    /**
     * lấy về tên của playlist
     * @return tên của playlist
     */
    protected String getName(){
        return name;
    }
    
    /**
     * Lấy ảnh nền của playlist
     * @return đường dẫn ảnh nền của playlist
     */
    protected String getBackground(){
        return srcImg;
    }
    
    /**
     * Lấy tất cả các track có trong playlist
     * @return list của tất cả track trong playlist
     */
    protected List<T> getListTrack(){
        return listTrack;
    }
    
    /**
     * Cài đặt tên cho playlist
     * @param name tên sẽ đặt cho playlist
     */
    protected void setName(String name){
        if(name.trim().compareTo("")!=0) this.name = name;
    }
    
    /**
     * Cài đặt ảnh nền cho playlist
     * @param src đường dẫn đến ảnh nền sẽ đặt cho pơlaylist
     */
    protected void setBackground(String src){
        if(src.trim().compareTo("")!=0) this.srcImg = src;
    }
    
    /**
     * Hủy chọn các track đang được chọn (trong chọn các track của playlist)
     */
    protected abstract void unSelectTracks();
    
    /**
     * Nhảy đến một một track T được xác định, hoặc play chính nó của PlaylistUser
     * @param t track sẽ được chuyển đến để chơi
     */
    protected abstract void seekTrack(T t);
    
    /**
     * Xóa đi một track cụ thể
     * @param obj track sẽ bị xóa khỏi danh sách
     * @return có xóa được hay không
     */
    protected boolean deleteTrack(T obj) {
        if(listTrack.contains(obj) && canDelete(obj)){
            int pos = listTrack.indexOf(obj);
            listTrack.remove(obj);
            deleteDisplay(obj, pos);
            obj = null;
            numbericList();
            return true;
        }else{
            Method.dialog("alert", "Sorry! Can not delete!");
            return false;
        }
    }
    
    /**
     * delete display of track at index from player display
     * @param pos index of track
     * @param hashcode hashcode of Track was deleted
     * @return deleted state
     */
    abstract protected boolean deleteDisplay(T t, int pos);
    
    /**
     * check this track can be deleted?
     * @param i is track want to delete
     * @return deleted state
     */
    abstract protected boolean canDelete(T i);
    
    /**
     * Xóa đồng thời nhiều track
     * @param listDelete danh sách các track sẽ bị xóa
     * @return có xóa thành công hay không
     */
    protected boolean deleteTracks(List<T> listDelete){
        int leng = listDelete.size(); T x;
        boolean canDel = true;
        
        for(short i=0; i < leng; ++i){
            x = listDelete.get(i);
            if(!listTrack.contains(x) || !canDelete(x)){
                Method.dialog("alert", "Can not delete because there is a track in the list which you want to delete is playing!");
                canDel = false;
            }
        }
        
        if(canDel){
            listDelete.forEach((t) -> {
                deleteTrack(t);
            });
            numbericList();
        }
        
        return canDel;
    }
    
    /**
     * Xóa bộ nhớ đệm sau khi đã load file media
     * @param player player đệm
     * @param md media đệm
     */
    protected void deleteTemp(MediaPlayer player, Media md){
        player.dispose();
        player = null;
        md = null;
    }
    
    /**
     * Sửa lại số thứ tự các track (sau khi xóa)
     */
    abstract protected void numbericList();
}