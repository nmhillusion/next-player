package nextplayer;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

/**
 *  Mẫu item được dùng để liệt kê trong danh sách bài hát
 * @author nmhillusion
 */

public class ItemRow extends HBox{
    private Label left, center, right;
    private short cntClick = 0;
    private boolean isSelected = false, isPlaying = false;
    
    /**
     * Khởi tạo cho ItemRow
     * @param _left chuỗi trái, thường là số thứ tự
     * @param _center chuỗi giữa, thường là tên
     * @param _right chuỗi phải, thường là thời gian hay số lượng tracks
     */
    public ItemRow(String _left, String _center, String _right) {
        super();
        left = new Label(_left);
        center = new Label(_center);
        right = new Label(_right);
    }
    
    /**
     * Cài đặt cho nó
     * @param w chiều rộng ứng dụng
     * @param h chiều cao ứng dụng
     * @param pl playlist chứa nó
     * @param ctxMenu context menu mà khi click phải lên nó sẽ xuất hiện
     * @param parent thứ mà nó đang đại diện
     * @return 
     */
    protected boolean setup(int w, int h, Playlist pl, ContextMenu ctxMenu, Object parent){
        setPrefSize(w, h); setSpacing(2);
        left.setPrefSize(50, 25);
        center.setPrefSize((w-50)*80/100, 25);
        right.setPrefSize((w-50)*20/100, 25);
        
        right.setTextAlignment(TextAlignment.RIGHT);
        
        setDefault();
        left.setFont(Method.getFont("general",10));
        center.setFont(Method.getFont("general",10));
        right.setFont(Method.getFont("general",10));
        
        setOnMouseMoved((event) -> {
            setHover();
        });
        
        setOnMouseExited((event) -> {
            if(!isPlaying){
                cntClick = 0;
                if(!isSelected){
                    setDefault();
                }
            }
        });
        
        setOnMousePressed((event) -> {
            if(!isPlaying && event.isPrimaryButtonDown()){
                if(++cntClick==2){
                    pl.seekTrack(parent);
                    cntClick = 0;
                    pl.unSelectTracks();
                }
            }
        });
        
        setOnMouseClicked((event) -> {
            if(event.isControlDown()){
                if(!isSelected)Selected();
                else unSelect();
            }else{
                if(!isSelected){
                    pl.unSelectTracks();
                }
            }
        });
        
        setOnContextMenuRequested((event) -> {
            if(!isSelected)Selected();
            ctxMenu.show(this, event.getScreenX() + 15, event.getScreenY());
        });
        
        return getChildren().addAll(left, center, right);
    }
    
    public void setTitle(String title){
        String centerStr = center.getText();
        center.setText(title + centerStr.substring(centerStr.indexOf(" | ")));
    }
    
    public void setArtist(String artist){
        String centerStr = center.getText();
        center.setText(centerStr.substring(0, centerStr.indexOf(" | ") + 3) + artist);
    }
    
    public void setDuration(String duration){
        right.setText(duration);
    }
    
    /**
     * @return có đang được chọn không
     */
    protected boolean isSelected(){
        return isSelected;
    }
    
    /**
     * 
     * @return có đang được chạy không
     */
    protected boolean isPlaying(){
        return isPlaying;
    }
    
    /**
     * cài đặt về trạng thái mặc định
     */
    protected void setDefault(){
        isPlaying = false; isSelected = false;
        left.setStyle("-fx-text-fill:#eff;-fx-background-color: rgba(34,34,34,0.5);-fx-padding: 0 0 0 10;-fx-border-color: #eff; -fx-border-width: 0 0 1 0;");
        center.setStyle("-fx-text-fill:#eff;-fx-background-color: rgba(34,34,34,0.5);-fx-padding: 0 0 0 10;-fx-border-color: #eff; -fx-border-width: 0 0 1 0;");
        right.setStyle("-fx-text-fill:#eff;-fx-background-color: rgba(34,34,34,0.5);-fx-padding: 0 0 0 10;-fx-border-color: #eff; -fx-border-width: 0 0 1 0;");
        left.setEffect(new InnerShadow(2, Color.valueOf("#36f")));
        center.setEffect(new InnerShadow(2, Color.valueOf("#36f")));
        right.setEffect(new InnerShadow(2, Color.valueOf("#36f")));
    }
    
    /**
     * cài đặt trạng thái khi vơ chuột ngang
     */
    private void setHover(){
        if(!isPlaying && !isSelected){
            left.setStyle("-fx-text-fill:#36f;-fx-background-color: transparent;-fx-border-color: #222 #222 #222 #f00;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 1 1 10;");
            left.setEffect(new InnerShadow(5, Color.CORAL));
            center.setStyle("-fx-text-fill:#36f;-fx-background-color: transparent;-fx-border-color: #222 #222 #222 #f00;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 1 1 1;");
            center.setEffect(new InnerShadow(5, Color.CORAL));
            right.setStyle("-fx-text-fill:#36f;-fx-background-color: transparent;-fx-border-color: #222 #222 #222 #f00;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 1 1 1;");
            right.setEffect(new InnerShadow(5, Color.CORAL));
        }
    }
    
    /**
     * chọn item này
     */
    private void Selected(){
        isSelected = true;
        if(!isPlaying){
            left.setStyle("-fx-background-color: #eff;-fx-text-fill: #36f;-fx-border-color: #eff #eff #eff #36f;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 0 1 6;");
            left.setEffect(new DropShadow(3, Color.AQUA));
            center.setStyle("-fx-background-color: #eff;-fx-text-fill: #36f;-fx-border-color: #eff #eff #eff #36f;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 0 1 0;");
            center.setEffect(new DropShadow(3, Color.AQUA));
            right.setStyle("-fx-background-color: #eff;-fx-text-fill: #36f;-fx-border-color: #eff #eff #eff #36f;-fx-border-style:solid;-fx-padding: 0 0 0 10;-fx-border-width: 1 1 1 0;");
            right.setEffect(new DropShadow(3, Color.AQUA));
        }
    }
    
    /**
     * Hủy chọn item này
     */
    protected void unSelect(){
        if(!isPlaying)setDefault();
        else setPlaying();
        isSelected = false;
    }
    
    /**
     * Cài đặt trạng thái khi đang được chơi
     */
    protected void setPlaying(){
        isPlaying = true;
        left.setStyle("-fx-text-fill:#eff;-fx-background-color: #359bed;-fx-padding: 0 0 0 10;-fx-border-color: #36f; -fx-border-width: 1 1 1 1;");
        left.setEffect(new InnerShadow(5, Color.WHITE));
        center.setStyle("-fx-text-fill:#eff;-fx-background-color: #359bed;-fx-padding: 0 0 0 10;-fx-border-color: #36f; -fx-border-width: 1 1 1 1;");
        center.setEffect(new InnerShadow(5, Color.WHITE));
        right.setStyle("-fx-text-fill:#eff;-fx-background-color: #359bed;-fx-padding: 0 0 0 10;-fx-border-color: #36f; -fx-border-width: 1 1 1 1;");
        right.setEffect(new InnerShadow(5, Color.WHITE));
    }
    
    /**
     * đặt lại số thứ tự cho item này
     * @param value số thứ tự muốn cài đặt cho item
     */
    protected void setNumber(String value){
        if(value.compareTo("null")!=0){
            left.setText(value);
        }
    }
}